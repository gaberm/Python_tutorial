# Import the necessary packages and modules
import matplotlib.pyplot as plt
import numpy as np

# # Prepare the data
# x = np.linspace(0, 10, 100)
# # Plot the data
# plt.plot(x, x, label='linear')
# # Add a legend
# plt.legend()
# # Show the plot
# plt.show()
# plt.scatter(x, x, label='linear')
# plt.angle_spectrum(x, x, label='linear')

# fig = plt.figure()
# ax = fig.add_subplot(111)
# ax.plot([1, 2, 3, 4], [10, 20, 25, 30], color='lightblue', linewidth=3)
# ax.scatter([0.3, 3.8, 1.2, 2.5], [11, 25, 9, 26], color='darkgreen', marker='*')
# ax.set_xlim(0.5, 4.5)
# plt.show()
#
#
# # Axes methods vs. pyplot
# plt.plot([1, 2, 3, 4], [10, 20, 25, 30], color='lightblue', linewidth=3)
# plt.scatter([0.3, 3.8, 1.2, 2.5], [11, 25, 9, 26], color='darkgreen', marker='^')
# plt.xlim(0.5, 4.5)
# plt.show()

## Multiple Axes
# fig, axes = plt.subplots(nrows=2, ncols=1)
# fig, axes = plt.subplots(nrows=2, ncols=2)
# plt.show()
## Note, if set to 1 column it will be a list, else it will be a numpy array
"""
fig, axes = plt.subplots(nrows=2, ncols=2)
axes[0,0].set(title='Upper Left')
axes[0,1].set(title='Upper Right')
axes[1,0].set(title='Lower Left')
axes[1,1].set(title='Lower Right')

# To iterate over all items in a multidimensional numpy array, use the `flat` attribute
# Remove all xticks and yticks...
for ax in axes.flat:
    # print(ax)
    ax.set(xticks=[], yticks=[])
plt.show()
"""

# fig = plt.figure()
# ax = fig.add_subplot(111)
# fig, ax = plt.subplots()

#Exercises..
import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0,10,100)
y1,y2,y3 = np.cos(x), np.cos(x+1), np.cos(x+2)
names = ['Signal 1', 'Signal 2', 'Signal 3']

fig, axes = plt.subplots(nrows=3, ncols=1)
for ax in axes.flat:
#     # print(ax)
    ax.set(xticks=[], yticks=[])

# axes[0].set_xlim(0,100)
# axes[1].set_xlim(0,100)
# axes[2].set_xlim(0,100)

axes[0].set(title=names[0], xlim=(0,100))
axes[1].set(title=names[1], xlim=(0,100))
axes[2].set(title=names[2], xlim=(0,100))


axes[0].plot(y1, color='red', linewidth=2, )
axes[1].plot(y2, color='red', linewidth=2)
axes[2].plot(y3, color='red', linewidth=2)
