import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
import numpy as np

# Fixing random state for reproducibility
np.random.seed(19680801)

fig, ax = plt.subplots()
ax.plot(100*np.random.rand(20))

formatter = ticker.FormatStrFormatter('$%1.2f')
ax.yaxis.set_major_formatter(formatter)

for tick in ax.yaxis.get_major_ticks():
    # tick.label1On = True
    # tick.label1.set_color('green')
    tick.label2On = True
    tick.label2.set_color('green')


plt.show()
