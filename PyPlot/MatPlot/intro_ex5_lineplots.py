import numpy as np
import matplotlib.pyplot as plt

linear_data = np.array([1,2,3,4,5,6,7,8])
exponential_data = linear_data**2

plt.figure()
# plot the linear data and the exponential data
plt.plot(linear_data, '-o', exponential_data, '-o')
# plt.plot(linear_data, '-s', exponential_data, '-s')

# plot another series with a dashed red line
# plt.plot([22,44,55], '--r')

plt.xlabel('Some data')
plt.ylabel('Some other data')
plt.title('A title')
# add a legend with legend entries (because we didn't have labels when we plotted the data series)
plt.legend(['Baseline', 'Competition', 'Us'])

## Add a shade between the graphs
plt.gca().fill_between( range( len( linear_data)), linear_data, exponential_data, facecolor = 'silver', alpha = 0.25)


# Another trial
import pandas as pd
linear_data = np.array([1,2,3,4,5,6,7,8])
exponential_data = linear_data**2

plt.figure()

observation_dates = np.arange('2018-01-01', '2018-01-09', dtype='datetime64[D]')
observation_dates = list( map(pd.to_datetime, observation_dates))

x = plt.gca().xaxis
# list(x.get_ticklabels())
for item in x.get_ticklabels():
    item.set_rotation(45)

plt.plot(observation_dates, linear_data, '-o', observation_dates, exponential_data, '-o')
plt.subplots_adjust(bottom=0.25)

ax = plt.gca()
ax.set_xlabel('Date')
ax.set_ylabel('Units')
ax.set_title('Exponential vs Linear Performance')

# Experimenting with LaTeX
ax.set_title('Quadratic ($x^2$) vs Linear ($x$) Performance')
