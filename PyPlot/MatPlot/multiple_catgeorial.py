import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
import numpy as np

names = ['group_a', 'group_b', 'group_c']
values = [1, 10, 100]

plt.figure(1, figsize=(9, 4))

plt.subplot(141)
plt.bar(names, values)
plt.subplot(142)
plt.scatter(names, values)
plt.subplot(143)
plt.plot(names, values)
plt.suptitle('Categorical Plotting')

plt.subplot(144)
# plt.stackplot(names, values)
# plt.step(names, values)

# plt.suptitle('Categorical Plotting')

plt.show()
