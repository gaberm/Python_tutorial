def my_plotter(ax, data1, data2, param_dict):
    """
    A helper function to make a graph

    Parameters
    ----------
    ax : Axes
        The axes to draw to

    data1 : array
       The x data

    data2 : array
       The y data

    param_dict : dict
       Dictionary of kwargs to pass to ax.plot

    Returns
    -------
    out : list
        list of artists added
    """
    out = ax.plot(data1, data2, **param_dict)
    return out

import numpy as np

# import matplotlib
# matplotlib.use('PS')   # generate postscript output by default
# matplotlib.use('gtk3agg')
# matplotlib.use('agg')
import matplotlib.pyplot as plt

# Example: which you would then use as:

data1, data2, data3, data4 = np.random.randn(4, 100)
# fig, ax = plt.subplots(1, 1)
# my_plotter(ax, data1, data2, {'marker': 'x', 'color':'green'})
# plt.show()

fig, (ax1, ax2) = plt.subplots(1, 2)

my_plotter(ax1, data1, data2, {'marker': 'x', 'color': 'red'})
my_plotter(ax2, data3, data4, {'marker': 'o'})

plt.show()
# plt.savefig('image.png')
