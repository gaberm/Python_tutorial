import matplotlib.pyplot as plt
import numpy as np

# x = []
# x = np.random.rand(4)
y = np.random.randint(1,10,10)
x = np.arange(10)

y2 = np.random.randint(1,10,10)
x2 = np.arange(10)

fig1 = plt.figure(figsize=(7,5), dpi=70, clear=True)
# fig1, ax1 = plt.subplots(figsize = (11, 5))
ax1 = fig1.add_subplot(211)
ax1.bar(x, y, label= 'blue bar')#, color = 'c')

ax2 = fig1.add_subplot(212)
ax2.bar(x2, y2, label= 'blue bar')#, color  = 'o')


# plt.xlabel('x')
# plt.ylabel('y')
# plt.title('Interesting Graph')
ax1.set_xlabel('X')
ax1.set_ylabel('Y')

# ax1.set_xticks()
ax1.set_title('Interesting Graph')
ax1.legend('Random',
          # title="Interesting Graph",
          loc="upper left")
          # bbox_to_anchor=(1, 0, 0.5, 1))
# ax1.legend(loc = 'upper right')
plt.show()
