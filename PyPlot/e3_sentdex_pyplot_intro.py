import matplotlib.pyplot as plt
import numpy as np

def BarPlot():
    x = [ 2,4,6,8,10]
    y = [6,7,8,2,4]
    x2 = [1,3,5,7,11]
    y2 = [7,8,2, 4,9]
    plt.bar(x,y, label='Bar1', color='orange')
    plt.bar(x2,y2, label='Bar2', color='darkblue')

    # plt.xlabel('Plot Number')
    # plt.ylabel('Important var')
    plt.title('Interesting\nCheck it out!')
    plt.legend()

BarPlot()

def LinePlot():
    x,y = [1,2,3] , [5,7,4]
    x2, y2 = [1,2,3], [10,14,12]
    plt.plot(x,y, label='first line')
    plt.plot(x2,y2, label = 'second line')
    plt.xlabel('Plot Number')
    plt.ylabel('Important var')
    plt.title('Interesting\nCheck it out!')
    plt.legend()
    # plt.show()

LinePlot()

def PopulationBars():
    population_age = np.random.randint(18, 120, 25)
    ids = [x for x in range(len(population_age))]
    labels=['IDs', 'Ages']
    plt.bar(ids, population_age, label = 'Ages')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('Interesting\nCheck it out!')
    plt.legend()

PopulationBars()

def HistBar():
    population_age = np.random.randint(0, 120, 25)
    population_age.sort(axis=0)
    print(population_age)
    # weighted = [0]*25
    # weighted[2]=0.5
    # weighted[3]=0.5

    bins = [0,10,20,30,40,50,60,70,80,90,100,120]  #Must Define
    plt.hist(population_age, bins, histtype = 'bar', rwidth=0.7)   #cumulative = True, weights = weighted, density = True
    # density = True return a normalized graph
    # weights       A

HistBar()

def HistStepFilled():
    population_age = np.random.randint(0, 120, 25)
    population_age.sort(axis=0)
    print(population_age)
    # weighted = [0]*25
    # weighted[2]=0.5
    # weighted[3]=0.5

    bins = [0,10,20,30,40,50,60,70,80,90,100,120]  #Must Define
    plt.hist(population_age, bins, histtype = 'stepfilled', rwidth=0.7)   #cumulative = True, weights = weighted, density = True
    # density = True return a normalized graph
    # weights       An array of weights, of the same shape as *x*.  Each value in *x* only contributes its associated weight towards the bin count

HistStepFilled()

def ScatterPlot():
    x=[1,2,3,4,5,6,7,8]
    y=[5,2,4,2,1,4,5,2]
    plt.scatter(x,y, label='skitscat', color='r', marker='.', s=45)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('Interesting\nCheck it out!')
    plt.legend(loc='lower right')

ScatterPlot()

def StackPlot():
    days = [1,2,3,4,5]
    sleeping=[7,8,6,11,7]
    eating = [2,3,4,3,2]
    working = [7,8,7,2,2,]
    playing = [8,5,7,8,13]

    # Workaround for labels..
    # plt.plot([],[], color='m', label='Sleeping')
    # plt.plot([],[], color='c', label='Eating')
    # plt.plot([],[], color='r', label='Working')
    # plt.plot([],[], color='k', label='Playing')
    plt.stackplot(days, sleeping, eating, working, playing, colors = ['m', 'c', 'r', 'k'], labels = ['Sleep', 'Eating', 'Working', 'Playing'])
    plt.xlabel('Days')
    plt.ylabel('Hours')
    plt.title('Interesting\nCheck it out!')
    plt.legend(loc='lower right')

StackPlot()

def PieChart():
    # days = [1,2,3,4,5]
    # sleeping=[7,8,6,11,7]
    # eating = [2,3,4,3,2]
    # working = [7,8,7,2,2,]
    # playing = [8,5,7,8,13]

    slices = [7,2,2,13]
    activities =['Sleep', 'Eating', 'Working', 'Playing']
    cols =  ['m', 'c', 'r', 'lightblue']
    plt.pie(slices, labels = activities,
        colors = cols, startangle = 90,
        shadow = True, explode =(0,0,0,0.1),
        autopct = '%1.1f%%')
    # plt.xlabel('Days')
    # plt.ylabel('Hours')
    plt.title('Interesting\nCheck it out!')
    plt.legend(loc='lower right')

PieChart()


import matplotlib.pyplot as plt
import numpy as np

MMA()
def MMA():

    # construct some data like what you have:
    x = np.random.randn(100, 8)
    mins = x.min(0)
    maxes = x.max(0)
    means = x.mean(0)
    std = x.std(0)

    # create stacked errorbars:
    # plt.errorbar(np.arange(8), means, std, fmt='ok', lw=3)
    # plt.errorbar(np.arange(8), means, [means - mins, maxes - means],
                 # fmt='.k', ecolor='gray', lw=1)
    fig, ax = plt.subplot()

    fig.plot(np.arange(8), means, std, lw=3)
    fig.plot(np.arange(8), means, [means - mins, maxes - means], lw=1)
    # plt.xlim(-1, 8)
