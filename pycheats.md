# Upgrade all outdated packages
pip install -U $(pip freeze | cut -d"=" -f1)

# Override entrypoint
docker run -it --entrypoint "/bin/bash" sentiments
