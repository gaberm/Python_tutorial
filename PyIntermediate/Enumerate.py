example = ['left', 'right', 'up', 'down']

# for i,j in range ( len (example)):
#     print(i, j)

for i,j in enumerate(example):
    print(i, j)

newdict = dict(enumerate(example))
[print(i,j) for i,j in enumerate(example)]
[print(i,j) for i,j in enumerate(newdict)]
[(i,j) for i,j in enumerate(newdict)]

[(i,j) for i,j in enumerate(newdict.values())]

# using Zip example
[(i,j) for i,j in zip(newdict.keys(), newdict.values())]
