import timeit

# print(timeit.timeit('1+3', number = 5000000))

# input_list = range(100)
#
# def div_by_5(num):
#     if num % 5 == 0:
#         return True
#     else:
#         return False
#
# xyz = (i for i in input_list if div_by_5(i))
# print(list(xyz))

print("\nNew run..")
print(timeit.timeit('''input_list = range(1000)

def div_by_5(num):
    if num % 5 == 0:
        return True
    else:
        return False

xyz = []
for i in input_list:
    if div_by_5(i):
        xyz.append(i)
''', number = 50000), 'Using a for loop!')

print(timeit.timeit('''input_list = range(1000)

def div_by_5(num):
    if num % 5 == 0:
        return True
    else:
        return False

xyz = (i for i in input_list if div_by_5(i))
''', number = 50000), 'Using a generator')

print(timeit.timeit('''input_list = range(1000)

def div_by_5(num):
    if num % 5 == 0:
        return True
    else:
        return False

xyz = [i for i in input_list if div_by_5(i)]
''', number = 50000), 'Using a list comperhension')

print(timeit.timeit('''input_list = range(1000)

def div_by_5(num):
    if num % 5 == 0:
        return True
    else:
        return False

xyz = (i for i in input_list if div_by_5(i))
xyz=list(xyz)
''', number = 50000), 'Using a generator and expanding using list()')


print(timeit.timeit('''input_list = range(1000)

def div_by_5(num):
    if num % 5 == 0:
        return True
    else:
        return False

xyz = (i for i in input_list if div_by_5(i))
for i in xyz:
    x = i
''', number = 50000), 'Using a generator and expanding using a loop')


# New run..
# 62.78829219899853 Using a for loop!
# 0.3845672930001456 Using a generator
# 61.88101703400025 Using a list comperhension
# 63.034139349001634 Using a generator and expanding using list()
# 64.07332852900072 Using a generator and expanding using a loop
