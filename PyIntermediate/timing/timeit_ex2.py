from time import sleep
from random import randrange

from itertools import islice, tee

nwise = lambda g, n=2: zip(*(islice(g, i, None) for i, g in enumerate(tee(l, n))))
windowed_avg = lambda g, n=2: (sum(xs)/len(xs) for xs in nwise(l,n))
# print(*windowed_avg())

from timeit import Timer
print(Timer('''.
(xs[:1] + xs[1:]) / 2
''', setup='''
from numpy.random import randint
xs = randint(0,10, size=1_000_1000)
''').timeit(number = 5))
