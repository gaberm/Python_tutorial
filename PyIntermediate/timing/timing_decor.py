import time

def timing(func):
    def wrap(f):
        time1 = time.time()
        ret = func(f)
        time2 = time.time()
        print('%s function took %0.3f ms' % (f.__name__, int((time2-time1)*1000.0)))
        return ret
    return wrap

def safe_div(x,y):
    if y==0: return 0
    return x/y

def try_div(x,y):
    try: return x/y
    except ZeroDivisionError: return 0

@timing
def test_many_errors(f):
    print("Results for lots of caught errors:")
    for i in range(1000000):
        f(i,0)

@timing
def test_few_errors(f):
    print("Results for no caught errors:")
    for i in range(1000000):
        f(i,1)

# test_many_errors(safe_div)
# test_many_errors(try_div)
# test_few_errors(safe_div)
# test_few_errors(try_div)

@timing
def printloop(x):
    y = 0
    for i in range(y):
        y+=1
    print('Done')

printloop(10000)

print(printloop.__name__)
