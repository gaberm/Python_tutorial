x = [1,2,3,4]
y = [5,6,7,8]
z = ['a', 'b', 'c', 'd']

for a,b in zip(z,y):
    print(a,b)

for a,b,c in zip(z,y,z):
    print(a,b)

print(zip(x,y,z))
print(list(zip(x,y,z)))

for i in zip(x,y,z):
    print(i)

dict(zip(x,z))
[print(a,b,c) for a,b,c in zip(x,y,z)]
