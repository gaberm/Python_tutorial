"""Python Generators || James Powell
Generators were a compelling feature when added to Python 2.2 in seventeen years ago with PEP-255.
When they were enhanced to become coroutines with PEP-342, they gained additional capabilities for modelling common problems.

But, surprisingly, we don't often see generators discussed as a core tool in the data scientist, computational
scientist, and data engineer's toolkit. Why not?

A Lecture in PyData London 2018
https://www.youtube.com/watch?v=XEn_99daJro"""

"""Section 1
def add1(x,y):
    return x + y

add2 = lambda x ,y : x + y
add2.__doc__ = 'add x to y'
add2.__name__ = 'add2'


add1(1, 4)
add2(1, 4)

help(add1)
help(add2)

class Adder:
    def __init__(self, z=0):
        self.z = z

    def __call__(self, x, y):
        # Added z as a counter and summed it to the result
        self.z += 1
        return x + y+ self.z

add3 = Adder()

print(f'add3: {add3(1,4)}')


def add1(x,y):
    'add x to y'
    #mimcing the z behavior
    add1.z += 1
    return x + y + add1.z

add1.z = 0
add1(1, 4)
add1.z
"""



"""Section 2: Compute example
from time import sleep
from random import randrange

def compute():
    sleep(.1)
    return randrange(100)

def f():
    rv = []
    for _ in range(10):
        rv.append(compute())
    return rv

print(f'Output of f is: {f()}')

class F:
    def __iter__(self):
        self.size = 10
        return self

    def __next__(self):
        if not self.size:
            raise StopIteration
        self.size -=1
        return compute()

    def __call__(self):
        rv = []
        for _ in range(10):
            rv.append(compute())
        return rv

f = F()
import timeit

f.__call__()
#Returning results via iteration
# f.__iter__()
# f.__next__()

print('-'* 80)

for x in f():
    print(x)
    break

### Behind the scene
# for x in xs:
#     print(x)
#
# xi = iter(xs)
# while True:
#     try:
#         x = next(xi)
#         print(x)
"""


""" Section 3 -
# Lazy vs Eager
# from 'localfolder' import compute #scriptname
# from time import sleep
# from random import randrange
#
# from itertools import islice, tee
#
# def compute(z=10):
#     sleep(.1)
#     return randrange(100)
#
# def f():
#     for i in range(10):
#         yield compute()
#
# for x in f():
#     print(x)
#
# l = [1, 2, 4, 5, 8, 11, 13, 15, 19, 21]
# tee(l, 2)
# for a,b in enumerate(tee(l, 3)):
#     print(a,b)
#
# nwise = lambda g, n=2: zip(*(islice(g, i, None) for i, g in enumerate(tee(l, n))))
# windowed_avg = lambda g, n=2: (sum(xs)/len(xs) for xs in nwise(l,n))
# print(*windowed_avg())
#
# for x in windowed_avg(f(), 3):
#     print(x)
#
#
# class F:
#     @staticmethod
#     def first():
#         print('first')
#     @staticmethod
#     def second():
#         print('second')
#     @staticmethod
#     def third():
#         print('third')
#
# f = F()
# f.first()
# f.second()(
# f.third()
#
# def f():
#     print('prequal')
#     yield
#     print('first')
#     yield
#     print('second')
#     yield
#     print('third')
#
# for i in f():
#     print(i)
#
# fi = f()
# next(fi)

# def task(msg):
#     while True:
#         yield
#         print(msg)
#
# def scheduler(*tasks):
#     while True:
#         for t in tasks:
#             next(t)
#
# scheduler(task('hello'), task('world'))
"""

"""Section 4 - Examples
from time import sleep
from random import randrange

from itertools import islice, tee

nwise = lambda g, n=2: zip(*(islice(g, i, None) for i, g in enumerate(tee(l, n))))
windowed_avg = lambda g, n=2: (sum(xs)/len(xs) for xs in nwise(l,n))
list(windowed_avg(xs,2))
# print(*windowed_avg())

from timeit import Timer

print(Timer('''
(xs[:1] + xs[1:]) / 2
''', setup='''
from numpy.random import randint
xs = randint(0,10, size=1_000_1000)
''').timeit(number = 5))

# results
# 0.7371237890038174
# 0.8322605579960509
# 0.8372520720004104
# 0.7407411610038253

print(Timer('''
list(windowed_avg(xs, 2))
''', setup ='''
from itertools import islice, tee
# from random import randrange
from numpy.random import randint

nwise = lambda g, n=2: zip(*(islice(g, i, None) for i, g in enumerate(tee(g, n))))
windowed_avg = lambda g, n=2: (sum(xs)/len(xs) for xs in nwise(g,n))
# xs = [randrange(0, 10) for _ in range(1_000_000)]
xs = randint(0,10, size=1_000_1000)
''').timeit(number = 5))

from random import randrange
from random import randint
xs =[ randrange(0,10) for _ in range(1_000_000) ]


from numpy.random import randint
xs = randint(0,10, size=1_000_1000)
xs[:1]
xs[1:]

"""

"""Section 5 - Fun


def term(value):
    while True:
        new_value = yield value
        if new_value is not None and new_value != value:
            value = new_value

def add(*terms):
    value = None
    while True:
        yield value
        value = sum(next(t) for t in terms)

eq = add(term(1), term(2), term(3))
while True:
    print(next(eq))


"""
