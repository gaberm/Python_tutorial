import pandas as pd
import numpy as np

s = pd.Series(range(5))
s.where(s > 3)
s.where(s > 3, s<1)
s.mask(s > 2)

df = pd.DataFrame(np.arange(10).reshape(-1, 2), columns=['A', 'B'])
m = df % 3 == 0
df.where(m, -df)

np.where(m, df, -df)

df.where(m, -df) == np.where(m, df, -df)
df.where(m, -df) == df.mask(~m, -df)


"""Pandas Slicing"""
dfl = pd.DataFrame(np.random.randn(5,4), columns=list('ABCD'), index=pd.date_range('20130101',periods=5))
dfl.iloc[2:3]


s = pd.Series(list('abcde'), index=[0,3,2,5,4])
s.loc[3:5]
s.sort_index().loc[1:6]
s.iloc[1:6]

dfl = pd.DataFrame(np.random.randn(5,2), columns=list('AB'))
dfl.iloc[:, 1:3]

"""Selection by Callable"""
df1 = pd.DataFrame(np.random.randn(6, 4),
                       index=list('abcdef'), columns=list('ABCD'))

df1.loc[lambda df: df.A > 0, :]
df1[['A', 'B']]
df1.loc[:, lambda df: ['A', 'B']]
df1.loc[:, lambda df: ['A', 'B']]

df1.iloc[:, lambda df: [0, 1]]
df1.iloc[:,[0, 1]]
df1[lambda df: df.columns[0]]
df1['A']

df1.A.loc[lambda s: s > 0]

df = pd.DataFrame(columns = list('abc'), data = np.random.randn(5,3))
df.mask(df.loc[df.index[[1,3,2]], 'a'])
x = df.loc[df.index[[1,3]], 'a']

index = pd.date_range(10)
import numpy as np
data = pd.DataFrame(data=[1,2,3,4,5,6,7,8,9,0], columns=['value'])
xdata = data.iloc[data.index[[1,2,4]]] * data
data.apply(filter(lambda x: data if xdata == np.nan else np.nan , xdata))
df = filter(lambda x: data if xdata == np.nan else np.nan , xdata)
zip(df)



""" Chained data Selection operations"""
bb = pd.read_csv('data/baseball.csv', index_col='id')
(bb.groupby(['year', 'team']).sum().loc[lambda df: df.r > 100])
