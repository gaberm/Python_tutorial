import pandas as pd
import matplotlib.pyplot as plt

north_america = pd.read_csv('./data/north_america_2000_2010.csv', index_col=0)
south_america = pd.read_csv('./data/south_america_2000_2010.csv', index_col=0)

americas = pd.concat([north_america, south_america])

americas_dfs = [americas]

for year in range(2011, 2016):
    filename = "./data/americas_{}.csv".format(year)
    df = pd.read_csv(filename, index_col=0)
    americas_dfs.append(df)

americas = pd.concat(americas_dfs, axis=1, sort=True)

# Other Contents
asia = pd.read_csv('./data/asia_2000_2015.csv', index_col=0)
europe = pd.read_csv('./data/europe_2000_2015.csv', index_col=0)
south_pacific = pd.read_csv('./data/south_pacific_2000_2015.csv', index_col=0)

world = americas.append([asia, europe, south_pacific])
# Historical
historical = pd.read_csv('./data/historical.csv', index_col=0)

world_historical = pd.merge(historical, world, left_index=True, right_index=True, how='right')

world_historical.sort_index(inplace=True)
world_historical.transpose().plot(figsize=(15,10), colormap='rainbow', linewidth=2, title='Average Labor Hours Per Year')
plt.legend(loc='right', bbox_to_anchor=(1.15, 0.5))
plt.show()

"""
pd.concat() function: the most multi-purpose and can be used to combine multiple DataFrames along either axis.
DataFrame.append() method: a quick way to add rows to your DataFrame, but not applicable for adding columns.
pd.merge() function: great for joining two DataFrames together when we have one column (key) containing common values.
DataFrame.join() method: a quicker way to join two DataFrames, but works only off index labels rather than columns.
"""
