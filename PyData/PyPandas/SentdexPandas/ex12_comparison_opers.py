import pandas as pd
import matplotlib.pyplot as plt
import numpy
from matplotlib import style

style.use('fivethirtyeight')

bridge_height = {'meters':[10.26, 10.31, 10.27, 10.22, 10.23, 6212.42, 10.28, 10.25, 10.31]}

df = pd.DataFrame(bridge_height)
df['STD'] = pd.DataFrame.rolling(df['meters'],2).std()

# df_std=df.describe(include='numpy.object')
# df['meters'].describe()
# numpy.percentile(df['meters'].describe(),[75])
# df_std
df_std=df.describe()['meters']['std']       ## NOTE: used to get rid of errornous data, an alternative would be using the 75% of the data describition
# type(df_std)
df = df[ (df['STD'] < df_std *1.5) ]
print(df)
df['meters'].plot(linewidth=2)
plt.show()
