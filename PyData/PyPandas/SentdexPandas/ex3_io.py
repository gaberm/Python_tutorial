import pandas as pd

# df = pd.read_csv('./ZILLOW-Z77006_MLPAH.csv')
# df.set_index('Date', inplace=True)
# print(df)
#
# df.to_csv('newcsv2.csv')
#
# df.columns = ['Date','Austin_HPI']

df = pd.read_csv('./ZILLOW-Z77006_MLPAH.csv', names=['Date', 'Austin_HPI'], index_col=0)
df.to_html('example.html')
