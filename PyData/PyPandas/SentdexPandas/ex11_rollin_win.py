### New version of Pandas:
#https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.rolling.html
import quandl
import pandas as pd
import pickle
import matplotlib.pyplot as plt
from matplotlib import style
style.use('fivethirtyeight')

# Not necessary, I just do this so I do not show my API key.
api_key = open('C:\\Users\\Mohammed\\gitlab\\quandlapikey.txt','r').read()

def state_list():
    fiddy_states = pd.read_html('https://simple.wikipedia.org/wiki/List_of_U.S._states')
    return fiddy_states[0][1][1:]


def grab_initial_state_data():
    states = state_list()

    main_df = pd.DataFrame()

    for abbv in states:
        query = "FMAC/HPI_"+str(abbv)
        df = quandl.get(query, authtoken=api_key)
        # print(query)
        df.columns = [abbv]  ### This is the fix ###
        df[abbv] = (df[abbv]-df[abbv][0]) / df[abbv][0] * 100.0
        # print(df.head())
        if main_df.empty:
            main_df = df
        else:
            main_df = main_df.join(df)

    pickle_out = open('fiddy_states3.pickle','wb', )
    pickle.dump(main_df, pickle_out)
    pickle_out.close()

def HPI_Benchmark():
    df = quandl.get("FMAC/HPI_USA", authtoken=api_key)
    df["United States"] = (df["United States"]-df["United States"][0]) / df["United States"][0] * 100.0
    return df

# HPI_Benchmark()
# grab_initial_state_data()


plt.figure(1)
ax1 = plt.subplot2grid((2,1), (0,0))
ax2 = plt.subplot2grid((2,1), (1,0), sharex=ax1)

plt.figure(2)
ax3 = plt.subplot2grid((2,1), (0,0))
ax4 = plt.subplot2grid((2,1), (1,0), sharex=ax3)


HPI_data = pd.read_pickle('fiddy_states3.pickle')
HPI_data['TX12MA'] = pd.DataFrame.rolling(HPI_data['TX'], 12).mean()
HPI_data['TX12STD'] = pd.DataFrame.rolling(HPI_data['TX'], 12).std()
TX_AK_12corr = HPI_data['TX'].rolling(window=12).corr(HPI_data['AK'])

# dir(pd.DataFrame.rolling)
# help(pd.DataFrame.rolling)

HPI_data.dropna(inplace=True)
# print(HPI_data[['TX', 'TX12MA','TX12STD']].head(20))

HPI_data[['TX','TX12MA']].plot(ax=ax1, linewidth=1)
HPI_data[['TX12STD']].plot(ax=ax2, linewidth=1)

HPI_data[['TX']].plot(ax=ax3, linewidth=1, label = 'TX HPI')
HPI_data[['AK']].plot(ax=ax3, linewidth=1, label = 'AK HPI')
TX_AK_12corr.plot(ax=ax4, label='TX_AK_12corr', linewidth=1, sharex=ax3)


""" For Training purposes """

# Working with multiple figure windows and subplots
import matplotlib.pyplot as plt
import numpy as np

t = np.arange(0.0, 2.0, 0.01)
s1 = np.sin(2*np.pi*t)
s2 = np.sin(4*np.pi*t)

plt.figure(3)
plt.subplot(211)
plt.plot(t, s1)
plt.subplot(212)
plt.plot(t, 2*s1)

plt.figure(4)
plt.plot(t, s2)

# now switch back to figure 1 and make some changes
plt.figure(3)
plt.subplot(211)
plt.plot(t, s2, 's')
ax = plt.gca()
ax.set_xticklabels([])

plt.legend(loc=4)
plt.show()
