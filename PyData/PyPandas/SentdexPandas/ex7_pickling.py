import quandl
import pandas as pd
import pickle
# import cPickle
import matplotlib as plt

# Not necessary, I just do this so I do not show my API key.
api_key = open('C:\\Users\\Mohammed\\gitlab\\quandlapikey.txt','r').read()


def state_list():
    fiddy_states = pd.read_html('https://simple.wikipedia.org/wiki/List_of_U.S._states')
    return fiddy_states[0][1][1:]

def grab_initial_state_data():
    states= state_list()
    main_df = pd.DataFrame()

    for abbv in states:
        query = "FMAC/HPI_"+str(abbv)
        df = quandl.get(query, authtoken=api_key)
        # df = df.pct_change()
        df.columns = [abbv]  ### This is the fix ###
        df[abbv] = (df[abbv] - df[abbv][0]) / (df[abbv] [0] * 100.0)
        # df[abbv] = lambda x: (x - df[abbv][0]) / (df[abbv][0] * 100.0)

        if main_df.empty:
            main_df = df
        else:
            main_df = main_df.join(df)
    return main_df

def HPI_Benchmark():
    df = Quandl.get("FMAC/HPI_USA", authtoken=api_key)
    df["United States"] = (df["United States"]-df["United States"][0]) / df["United States"][0] * 100.0
    return df


    # print(main_df)
##    pickle_out = open('fiddy_states.pickle','wb')
##    pickle.dump(main_df, pickle_out)
##    pickle_out.close()
#
HPI_data=grab_initial_state_data()
# benchmark = HPI_benchmark()
# # HPI_data['TX2']=HPI_data['TX'] *2
#
##pickle_in = open('fiddy_states.pickle','rb')
##HPI_data = pickle.load(pickle_in)
# print(HPI_data)
#
# ### Pandas's version
# # HPI_data.to_pickle('pickle.pickle')
# # HPI_data2=pd.read_pickle('pickle.pickle')
#
# # print(HPI_data[['TX','TX2']])
# # print(HPI_data['TX2'])
# fig = plt.figure()
# ax1 = plt.subplot2grid((1,1), (0,0))
# HPI_data.plot(ax = ax1)
# benchmark.plot(ax = ax1, color='k', linewidth=7)
# # HPI_data.plot()

# HPI_state_corr = HPI_data.corr()
# print(HPI_state_corr)from numpy.random import randint

fig = plt.pyplot.figure
# ax1 = plt.subplot2grid((1,1), (0,0))
ax1= plt.pyplot.subplot2grid((1,1), (0,0), rowspan=1, colspan=1, fig=None)
# HPI_data['TX'].plot(ax = ax1)
# benchmark.plot(ax = ax1, color='k', linewidth=7)

HPI_data['TX1yr'] = HPI_data['TX'].resample('A').mean()

# TX1yr.plot(label='Yearly TX HPI')
# Handling missing data.
# HPI_data.dropna( inplace=True)      # Dropping every row with the missing data.
# HPI_data.fillna(method='ffill', inplace=True)      # Filling every row with the missing data.
HPI_data.fillna(method='bfill', inplace=True)      # Filling every row with the missing data.
# HPI_data.fillna(value=-9999, inplace=True)      # Filling every row with the a value.
HPI_data[['TX','TX1yr']].plot(ax=ax1)
print(HPI_data[['TX','TX1yr']])

plt.pyplot.legend(loc=4)
plt.pyplot.show()
