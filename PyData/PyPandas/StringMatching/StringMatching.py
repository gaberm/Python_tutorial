''.join(x_set)
''.join(y_set)
import difflib
d=difflib.diff_bytes(None, x_set, y_set)
d=difflib.SequenceMatcher(None, x_set, y_set)
s=difflib.SequenceMatcher(None, ''.join(x_set), ''.join(y_set) )
s.ratio()

x="Your Google verification code is XX"
x=x.lower()
y="G-XX is your Google verification code."
y=y.lower()
difflib.SequenceMatcher(None, x, y).ratio()
%timeit difflib.SequenceMatcher(None, x, y).quick_ratio()       #219u
%timeit difflib.SequenceMatcher(None, x, y).real_quick_ratio()  #104u

from fuzzywuzzy import fuzz, process
dir(fuzz)

fuzz.QRatio(x,y)
fuzz.ratio(x,y)
fuzz.token_set_ratio(x,y)
fuzz.token_sort_ratio(x,y)
%timeit fuzz.UWRatio(x,y)   #274u   - 95
%timeit fuzz.WRatio(x,y)    #317u  - 95
fuzz.UQRatio(x,y)   #81u

%timeit -r100 process.default_scorer(x,y)  #319u - 95


import string
import functools
s = "string. With. Punctuation?" # Sample string
out = s.translate(maketrans("",""), string.punctuation)

s.translate(string.punctuation)









import re, string, timeit

s = "string. With. Punctuation"
exclude = set(string.punctuation)
table = str.maketrans("xx","yy", string.punctuation)
regex = re.compile('[%s]' % re.escape(string.punctuation))

def test_set(s):
    return ''.join(ch for ch in s if ch not in exclude)

def test_re(s):  # From Vinko's solution, with fix.
    return regex.sub('XX', s)

def test_trans(s):
    return s.translate(str.maketrans('WP', 'wp',string.punctuation))

def test_repl(s):  # From S.Lott's solution
    for c in string.punctuation:
        s=s.replace(c,"")
    return s


print( "sets      :",timeit.Timer('f(s)', 'from __main__ import s,test_set as f').timeit(1000000))      #sets      : 25.376244796003448
print( "regex     :",timeit.Timer('f(s)', 'from __main__ import s,test_re as f').timeit(1000000))       #regex     : 7.981173829000909
print( "translate :",timeit.Timer('f(s)', 'from __main__ import s,test_trans as f').timeit(1000000))    #translate : 25.11143899500894
print( "translate :",timeit.Timer('f(s)', 'from __main__ import s,test_trans2 as f').timeit(1000000))    #translate : 14.686272257997189
print( "replace   :",timeit.Timer('f(s)', 'from __main__ import s,test_repl as f').timeit(1000000))     #replace   : 54.91191715300374


def test_trans2(s):
    return s.translate(table)

test_trans(s)
test_trans(x)
test_trans2(x)
test_trans2(y)

# yourstring.translate(str.maketrans(fromstr, tostr, deletestr))
# Replace the characters in fromstr with the character in the same position in tostr and delete all characters that are in deletestr. The fromstr and tostr can be empty strings and the deletestr parameter can be omitted.
str="preetideepak12345aeiou"
str.translate(str.maketrans('abcde','12345','p'))

import re
regex = re.compile('[0-9]{3,}')
x="your google verification co\nde is 8817"
test_re(x)
table = str.maketrans("",'',"\n")
x.translate(table)
x.translate(str.maketrans("",'',"\n"))
# array.Message = array.Message.replace('[0-9]{3,}', 'XX',regex=True).astype(str)
# array.Message = array.Message.str.replace(r'\n',' ',regex=True).astype(str)  # RegEx substraction -[._]
