import numpy as np
import pandas as pd

pd.Series(5., index=['a', 'b', 'c', 'd', 'e'])
s = pd.Series(np.random.randn(5), index=['a', 'b', 'c', 'd', 'e'])
s[0]
s[:3]
s.median()
s[s>= s.median()]
np.exp(s)
s*3
np.frexp(s)
s[0:][1]
s['e']
s.get('f', np.nan)
s = pd.Series(np.random.randn(5), name='something')
d = {'one' : pd.Series([1., 2., 3.], index=['a', 'b', 'c' ]),
     'two' : pd.Series([1., 2., 3., 4.], index=['a', 'b', 'c', 'd'])}
d
df = pd.DataFrame(d)
df
df.index
df.columns
data = np.zeros((3,), dtype=[('A', 'i4'),('B', 'f4'),('C', 'a10')])
data[:]
pd.DataFrame(data)



msgdata = pd.read_csv(fname, usecols=['Message', 'Source'] )
text.Message.drop(labels=i, axis=0, level=None, inplace=True, errors='raise')

row = pd.Series({'Source':"NASA",'Message':"Instructions for aggregation are sent"})
msgdata.append(row, ignore_index=True)
msgdata.append({'Source':"Hubl", "Message": "I can see you"}, ignore_index=True)

pd.DataFrame.drop([26,25,2,3,4,5], axis=0)
msgdata.drop([26,25,2,3,4,5], axis=0, inplace = True)
