import numpy as np

# muli-array example
arr = np.arange(36).reshape(3,4,3)

arr[0]
arr[0,0]
arr[0,0,2]

# Counting value in a vector
np.random.seed(444)
x= np.random.choice([False, True], size = 100000)
x= np.random.choice([False, True], size = 10)

zip(x[:1],x[:-1])

def count_transitions(x)->int:
    count = 0
    for i, j in zip(x[:-1], x[1:]):
        print(i,j)
        if i != j:
            count +=1
    return count

import timeit
%timeit
count_transitions(x)

print(x)
%timeit
np.count_nonzero(x[:-1] != x[1:] )


arr[0,1]
arr[0,1][:-1]
arr[0,1][1:]


# Buy Low, Sell High
# Here’s another example to whet your appetite. Consider the following classic technical interview problem:
#
# Given a stock’s price history as a sequence, and assuming that you are only allowed to make one purchase and one sale, what is the maximum profit that can be obtained? For example, given prices = (20, 18, 14, 17, 20, 21, 15), the max profit would be 7, from buying at 14 and selling at 21.

def profit(prices):
     max_px = 0
     min_px = prices[0]
     for px in prices[1:]:
         min_px = min(min_px, px)
         max_px = max(px - min_px, max_px)
     return max_px

prices = (20, 18, 14, 17, 20, 21, 15)
profit(prices)      ##7

## Using numpy
# Create mostly NaN array with a few 'turning points' (local min/max).
prices = np.full(100, fill_value=np.nan)
prices[[0, 25, 60, -1]] = [80., 30., 75., 50.]

# Linearly interpolate the missing values and add some noise.
x = np.arange(len(prices))
is_valid = ~np.isnan(prices)
prices = np.interp(x=x, xp=x[is_valid], fp=prices[is_valid])
prices += np.random.randn(len(prices)) * 2

## Here’s what this looks like with matplotlib. The adage is to buy low (green) and sell high (red):
import matplotlib.pyplot as plt

mn = np.argmin(prices)
mx = mn + np.argmax(prices[mn:])
dummy()
def dummy():
    kwargs = {'markersize': 12, 'linestyle': ''}
    fig, ax = plt.subplots()
    ax.plot(prices)
    ax.set_title('Price History')
    ax.set_xlabel('Time')
    ax.set_ylabel('Price')
    ax.plot(mn, prices[mn], color='green', **kwargs)
    ax.plot(mx, prices[mx], color='red', **kwargs)

    plt.show()



### Intermezzo: Understanding Axes Notation
# In NumPy, an axis refers to a single dimension of a multidimensional array:

arr = np.array([[1, 2, 3], [10, 20, 30]])
arr.sum(axis=0)
# array([11, 22, 33])
arr.sum(axis=1)
# array([ 6, 60])
# axis : {'index' (0), 'columns' (1)}



### Broadcasting
a = np.array([1.5, 2.5, 3.5])
b = np.array([10., 5., 1.])
a/b

# The term broadcasting describes how NumPy treats arrays with different shapes during arithmetic operations. Subject to certain constraints, the smaller array is “broadcast” across the larger array so that they have compatible shapes. Broadcasting provides a means of vectorizing array operations so that looping occurs in C instead of Python. [source]

sample = np.random.normal(loc=[2., 20.], scale=[1., 4],
...                           size=(3, 2))
sample.shape

mu = sample.mean(axis=0)
sample.std()
sample.min(axis=1)[:, None]  # 3 minimums across 3 rows
[*sample.min(axis=1)]
sample - sample.min(axis=1)[:, None]
print('sample:', sample.shape, '| means:', mu.shape)    ##sample: (3, 2) | means: (2,)

arr
arr.mean(axis=1)


sample - mu

(sample - sample.mean(axis=0)) / sample.std(axis=0)


## Array Programming
tri = np.array([[1, 1],
...                 [3, 1],
...                 [2, 3]])

centroid = tri.mean(axis=0)

dummy()
def dummy():
    trishape = plt.Polygon(tri, edgecolor='r', alpha=0.3, lw=5)
    _, ax = plt.subplots(figsize=(6, 6), dpi=100)
    ax.add_patch(trishape)
    ax.set_ylim([.5, 3.5])
    ax.set_xlim([.5, 3.5])
    ax.scatter(*centroid, color='r', marker='D', s=70)
    ax.scatter(*tri.T, color='b',  s=70)
    x = np.sum(tri**2, axis=1) ** 0.5
    X = np.repeat([[5, 5], [10, 10]], [5, 5], axis=0)
    tri2 = plt.Polygon(X,  alpha=0.3, lw=5)
    ax.add_patch(tri2)



import pandas as pd

def balance(pv, rate, nper, pmt) -> np.ndarray:
    d = (1 + rate) ** nper  # Discount factor
    return pv * d - pmt * (d - 1) / rate


freq = 12     # 12 months per year
rate = .0675  # 6.75% annualized
nper = 30     # 30 years
pv = 200000   # Loan face value

rate /= freq  # Monthly basis
nper *= freq  # 360 months

periods = np.arange(1, nper + 1, dtype=int)
principal = np.ppmt(rate, periods, nper, pv)
interest = np.ipmt(rate, periods, nper, pv)
pmt = principal + interest  # Or: pmt = np.pmt(rate, nper, pv)

cols = ['beg_bal', 'prin', 'interest', 'end_bal']

data = [balance(pv, rate, periods - 1, -pmt),
        principal,
        interest,
        balance(pv, rate, periods, -pmt)]

table = pd.DataFrame(data, columns=periods, index=cols).T
table.index.name = 'month'

with pd.option_context('display.max_rows', 60):
    # Note: Using floats for $$ in production-level code = bad
    print(table.round(2))


pd.set_option('large_repr', 'truncate')
pd.set_option('display.max_rows', 60)
pd.set_option('max_colwidth', 10)
