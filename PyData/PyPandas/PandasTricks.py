# https://realpython.com/python-pandas-tricks/#2-make-toy-data-structures-with-pandas-testing-module
tm.N, tm.K = 30, 3  # Module-level default rows/columns

import numpy as np
np.random.seed(444)

tm.makeTimeDataFrame(freq='M').head()
tm.makeDataFrame().head()
tm.makeMissingDataframe()
dir(tm)

# Take Advantage of Accessor Methods
import pandas as pd
pd.Series._accessors

addr = pd.Series([
     'Washington, D.C. 20003',
     'Brooklyn, NY 11211-1755',
    'Omaha, NE 68154',
    'Pittsburgh, PA 15211'])

addr.str.upper()
addr.str.count(r'\d')  # 5 or 9-digit zip?

# For a more involved example, let’s say that you want to separate out the three city/state/ZIP components neatly into DataFrame fields.
regex = (r'(?P<city>[A-Za-z ]+), '      # One or more letters
          r'(?P<state>[A-Z]{2}) '        # 2 capital letters
          r'(?P<zip>\d{5}(?:-\d{4})?)')  # Optional 4-digit extension

regex = (r'(?P<city>[A-Za-z ]+), ', r'(?P<state>[A-Z]{2}) ', r'(?P<zip>\d{5}(?:-\d{4})?)')
addr.str.replace('.', '').str.extract(regex)
addr.str.extract(regex)

"""Each accessor is itself a bona fide Python class:

.str maps to StringMethods.
.dt maps to CombinedDatetimelikeProperties.
.cat routes to CategoricalAccessor.
"""
daterng = pd.Series(pd.date_range('2017', periods=9, freq='Q'))
daterng.dt.day_name()
# Second-half of year only
daterng[daterng.dt.quarter > 2]
daterng[daterng.dt.is_year_end]

# The third accessor, .cat, is for Categorical data only, which you’ll see shortly in its own section.

# Create a DatetimeIndex From Component Columns
import numpy as np
from itertools import product
datecols = ['year', 'month', 'day']

df = pd.DataFrame(list(product([2017, 2016], [1, 2], [1, 2, 3])),
                   columns=datecols)
df['data'] = np.random.randn(len(df))
df.index = pd.to_datetime(df[datecols])
df = df.drop(datecols, axis=1).squeeze()

df.index.dtype_str


# Use Categorical Data to Save on Time and Space
colors = pd.Series([
     'periwinkle',
     'mint green',
     'burnt orange',
     'periwinkle',
     'burnt orange',
     'rose',
     'rose',
     'mint green',
     'rose',
     'navy'
])
import sys
colors.apply(sys.getsizeof)
mapper = {v: k for k, v in enumerate(colors.unique())}
colors.unique
mapper
as_int = colors.map(mapper)
as_int.apply(sys.getsizeof)
#Another way to do it, returns a list
pd.factorize(colors)[0]
colors['factorized'] = pd.factorize(colors)[0]


# Not a huge space-saver to encode as Categorical
colors.memory_usage(index=False, deep=True)
colors.astype('category').memory_usage(index=False, deep=True)

ccolors = colors.astype('category')
ccolors.cat.categories
ccolors.cat.categories['navy']
ccolors.cat.categories[2]

ccolors.cat.reorder_categories(mapper).cat.codes

#Explore categories
[i for i in dir(ccolors.cat) if not i.startswith('_')]

ccolors.iloc[5] = 'a new color'
# ... ValueError: Cannot setitem on a Categorical with a new category, set the categories first

>>> ccolors = ccolors.cat.add_categories(['a new color'])
>>> ccolors.iloc[5] = 'a new color'  # No more ValueError
