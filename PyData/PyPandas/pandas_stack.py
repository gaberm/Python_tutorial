import pandas
import scipy

df = pandas.DataFrame({"s1_x": scipy.randn(10), "s1_y": scipy.randn(10), "s2_x": scipy.randn(10), "s2_y": scipy.randn(10)})
# id = df.loc[:, ['s2_y']]
df.columns = pandas.MultiIndex.from_tuples([tuple(c.split('_')) for c in df.columns])
print(df)
df=df.stack(0).reset_index(1) #reindex based on levels in DataFrame
print(df)
