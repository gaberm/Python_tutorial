# https://pandas.pydata.org/pandas-docs/version/0.23/groupby.html
"""Group By: split-apply-combine

By “group by” we are referring to a process involving one or more of the following steps

Splitting the data into groups based on some criteria
Applying a function to each group independently
Combining the results into a data structure"""

import pandas as pd
import numpy as np

df = pd.DataFrame({'A' : ['foo', 'bar', 'foo', 'bar', 'foo', 'bar', 'foo', 'foo'], 'B' : ['one', 'one', 'two', 'three', 'two', 'two', 'one', 'three'], 'C' : np.random.randn(8),  'D' : np.random.randn(8)})

grouped = df.groupby('A')
type(grouped)
grouped = df.groupby(['A', 'B'])

df.groupby(['A', 'B'])

def get_letter_type(letter):
    if letter.lower() in 'aeiou':
        'vowel'
    else:
        'consonant'

df
grouped = df.groupby(get_letter_type, axis=1)

print(grouped)
grouped.all()
grouped.first()
grouped.groups

# Grouping by duplicate indexing..
lst = [1, 2, 3, 1, 2, 3]
s = pd.Series([1, 2, 3, 10, 20, 30], lst)
grouped = s.groupby(level=0)


grouped.first()
grouped.all()
grouped.last()
grouped.sum()


#Groupby Sorting
df2 = pd.DataFrame({'X' : ['B', 'B', 'A', 'A'], 'Y' : [1, 2, 3, 4]})

df2.groupby(['X']).first()
df2.groupby(['X']).last()
df2.groupby(['X']).all()

df2.groupby(['X']).sum()
df2.groupby(['X']).count()

df2.groupby(['X'], sort=False).sum()

#By Default, Groupby perserves the order of observations
df3 = pd.DataFrame({'X' : ['A', 'B', 'A', 'B'], 'Y' : [1, 4, 3, 2]})

df3.groupby(['X']).get_group('A')
df3.groupby(['X']).get_group('B')

# GroupBy object attributes
# The groups attribute is a dict whose keys are the computed unique groups and corresponding values being the axis labels belonging to each group.
df
df.groupby('A').groups
df.groupby('B').groups
df.groupby('B').get_group('one')
df.groupby(['A', 'B']).groups
df.groupby(['A', 'B']).get_group(('bar','one'))
df.groupby(get_letter_type, axis=1)


df = pd.date_range(start='1/1/2000', end='01/10/2000', freq='d')

x = ['Date',    'height',      'weight',  'gender']
df = pd.DataFrame(x)
df.T
x =[
'2000-01-01', 42.849980, 157.500553,  'male',
'2000-01-02', 49.607315, 177.340407,  'male',
'2000-01-03', 56.293531, 171.524640,  'male',
'2000-01-04', 48.421077, 144.251986, 'female',
'2000-01-05', 46.556882, 152.526206,  'male',
'2000-01-06', 68.448851, 168.272968, 'female',
'2000-01-07', 70.757698, 136.431469,  'male',
'2000-01-08', 58.909500, 176.499753, 'female',
'2000-01-09', 76.435631, 174.094104, 'female',
'2000-01-10', 45.306120, 177.540920,   'male'
]
import numpy as np
df = pd.DataFrame(np.reshape(x, (10,4)))
df.columns= ['Date',    'height',      'weight',  'gender']

gb=df.groupby('gender')
dir(gb)

 arrays = [['bar', 'bar', 'baz', 'baz', 'foo', 'foo', 'qux', 'qux'],
 ['one', 'two', 'one', 'two', 'one', 'two', 'one', 'two']]

index = pd.MultiIndex.from_arrays(arrays, names=['first', 'second'])
s = pd.Series(np.random.randn(8), index=index)

grouped = s.groupby(level=0)
grouped.sum()
grouped = s.groupby(level='second').sum()

"""Transformation
The transform method returns an object that is indexed the same (same size) as the one being grouped. """
import pandas as pd; import numpy as np

index = pd.date_range('10/1/1999', periods=1100)
ts = pd.Series(np.random.normal(0.5, 2, 1100), index)
ts = ts.rolling(window=100,min_periods=100).mean().dropna()

key = lambda x: x.year
zscore = lambda x: (x - x.mean()) / x.std()
transformed = ts.groupby(key).transform(zscore)

grouped = ts.groupby(key)
grouped.mean()
transformed.mean()
grouped_trans = transformed.groupby(key)
grouped_trans.mean()

compare = pd.DataFrame({'Original': ts, 'Transformed': transformed})
compare.plot()


data_range = lambda x: x.max() - x.min()
ts.groupby(key).transform(data_range)
ts.groupby(key).transform('max') - ts.groupby(key).transform('min')
