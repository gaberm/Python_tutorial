# https://stackoverflow.com/questions/51428216/how-to-determine-num-entries-per-minute-in-pandas-data
# How to determine num entries per minute in pandas data
import pandas as pd
import datetime as dt

#Frame creation
f= ['f0001',
        'f0001',
        'f0001',
        'f0001',
        'f0020',
        'f0008',
        'f0001',
        'f0005',
        'f3203',
        'f0002',
        'f0002',
        'f0001',
        'f0201',
        'f0001',
        'f0439',
        'f0233',
        'f0008',
        'f0003',
        'f0009',
        'f0005']

dates = ['20130101100103', '20130101100110',
               '20130101100125', '20130101100133',
               '20130101100100', '20130101100200',
               '20130101100200', '20130101100200',
               '20130101100200', '20130101100200',
               '20130101100200', '20130101100300',
               '20130101100300', '20130101100300',
               '20130101100300', '20130101100400',
               '20130101100400', '20130101100400',
               '20130101100400', '20130101100400']

d = {'date':  dates}
data = pd.DataFrame(d)
data['user'] = f

#Time formatting
data.date = data.date.apply(str)
data.date = data.date.apply(lambda x:
dt.datetime.strptime(x,'%Y%m%d%H%M%S'))

s = data.groupby([data.date.map(lambda t: t.minute)]).count()
data.groupby([data.date.map(lambda t: t.minute), data['user']]).count()
data.groupby([data.date.dt.minute, data['user']]).count()
df = data
df.groupby([data.date.dt.minute]).count()
data.groupby([data.date.dt.minute, data['user']]).count().rename(columns={'date':'count'}).reset_index()

data.groupby(pd.Grouper(key='date', freq='min'))['user'].value_counts()
data.groupby(pd.Grouper(key='date', freq='min'))['user'].count()


from random import randint
rng = pd.date_range('1/1/2011', periods=10000, freq='1Min')
df = pd.DataFrame(randint(size=len(rng), low=100, high = 500), index=rng)
df.columns = ['process_id']
