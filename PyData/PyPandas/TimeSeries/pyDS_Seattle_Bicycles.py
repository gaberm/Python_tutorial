## Python Data Science Handbook - Seattle Bicyclke Counts

# https://data.seattle.gov/api/views/65db-xm6k/rows.csv?accessType=DOWNLOAD
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_csv("https://data.seattle.gov/api/views/65db-xm6k/rows.csv?accessType=DOWNLOAD", index_col='Date', parse_dates=True)

data.columns=['West', 'East']
# data['Total'] = data.eval(data['West'] + data['East'])
data['Total'] = data['West'] + data['East']


# !pip install plotly.express
# !pip install plotly --upgrade
# !pip install chart_studio

# Import Plotly
# import plotly
# import plotly.express as px
# import plotly.graph_objs as go
# import chart_studio.tools as tls
# from plotly.offline import init_notebook_mode
# plotly.offline.init_notebook_mode(connected=True)

daily = data.resample('D').sum()
daily.head()
daily.rolling(30, center=True).sum().plot(style=[':', "--", "-"])

daily.rolling(50, center=True, win_type='gaussian').sum(std=10).plot(style=[':', "--", "-"])

by_time = data.groupby(data.index.time).mean()
hourly_ticks = 4 * 60 * 60 * np.arange(6)
by_time.plot(xticks=hourly_ticks, style=[':', '--', '-'])

by_weekday = data.groupby(data.index.dayofweek).mean()
by_weekday.index = ['Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat', 'Sun']
by_weekday.plot(style=[':', '--', '-']);

weekend = np.where(data.index.weekday < 5, 'Weekday', 'Weekend')
by_time = data.groupby([weekend, data.index.time]).mean()

def ploto():
    weekend = np.where(data.index.weekday < 5, 'Weekday', 'Weekend')
    by_time = data.groupby([weekend, data.index.time]).mean()

    fig, ax = plt.subplots(1, 2, figsize=(14, 5))
    by_time.loc['Weekday'].plot(ax=ax[0], title='Weekdays', xticks=hourly_ticks, style=[':', '--', '-'])
    by_time.loc['Weekend'].plot(ax=ax[1], title='Weekends', xticks=hourly_ticks, style=[':', '--', '-']);

ploto()

data.head()
