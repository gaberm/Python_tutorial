# https://medium.com/@stallonejacob/time-series-forecast-a-basic-introduction-using-python-414fcb963000
import pandas as pd
import numpy as np
import matplotlib.pylab as plt

data = pd.read_csv('https://raw.githubusercontent.com/AileenNielsen/TimeSeriesAnalysisWithPython/master/data/AirPassengers.csv')
print (data.head())
print ('\n Data Types:')
print (data.dtypes)

def plot():
    data.plot()

plot()

# The data contains a particular month and number of passengers travelling in that month .The data type here is object (month) Let’s convert it into a Time series object and use the Month column as our index.
from datetime import datetime
con=data['Month']
data['Month']=pd.to_datetime(data.index)
data.set_index('Month', inplace=True)
data.index
data.head()

# You can see that now the data type is ‘datetime64[ns]’.Now let’s just make it into a series rather than a data frame ( this would make it easier for the blog explanation )
#convert to time series:
ts = data['#Passengers']
ts.head(10)

#1. Specific index
ts['1949-01-01']

#2. Import datetime and use 'datetime' function
from datetime import datetime
ts[datetime(1949,1,1)]

# 3. Get Range
# 3.1 Entire Range
ts['1949-01-01' : '1959-01-01']

#3.2 End boundary
ts[:'1949-10-01']

#3.3 All row of 1962
ts['1951']


## STATIONARITY

# This is a very important concept in Time Series Analysis. In order to apply a time series model, it is important for the Time series to be stationary; in other words all its statistical properties (mean,variance) remain constant over time. This is done basically because if you take a certain behavior over time, it is important that this behavior is same in the future in order for us to forecast the series. There are a lot of statistical theories to explore stationary series than non-stationary series. (Thus we can bring the fight to our home ground!)

# In practice we can assume the series to be stationary if it has constant statistical properties over time and these properties can be:
# • constant mean
# • constant variance
# • an auto co-variance that does not depend on time.


plt.plot(ts)

from statsmodels.tsa.stattools import adfuller

def test_stationarity(timeseries):

    # Dropping NA
    timeseries.dropna(inplace = True)
    #Determing rolling statistics
    rolmean = pd.DataFrame.rolling(timeseries, window = 12).mean()
    rolstd = pd.DataFrame.rolling(timeseries, window=12).std()

#Plot rolling statistics:
    plt.plot(timeseries, color='blue',label='Original')
    plt.plot(rolmean, color='red', label='Rolling Mean')
    plt.plot(rolstd, color='black', label = 'Rolling Std')
    plt.legend(loc='best')
    plt.title('Rolling Mean & Standard Deviation')
    plt.show()
    #Perform Dickey-Fuller test:
    print ('\nResults of Dickey-Fuller Test:')
    dftest = adfuller(timeseries, autolag='AIC')
    dfoutput = pd.Series(dftest[0:4], index=['Test Statistic','p-value','#Lags Used','Number of Observations Used'])
    for key,value in dftest[4].items():
        dfoutput['Critical Value (%s)'%key] = value
    print (dfoutput)


test_stationarity(ts)

# This is not stationary because :
# • mean is increasing even though the std is small.
# • Test stat is > critical value.
# • Note: the signed values are compared and the absolute values.


## MAKING THE TIME SERIES STATIONARY
# There are two major factors that make a time series non-stationary. They are:
# • Trend: non-constant mean
# • Seasonality: Variation at specific time-frames

# The basic idea is to model the trend and seasonality in this series, so we can remove it and make the series stationary. Then we can go ahead and apply statistical forecasting to the stationary series. And finally we can convert the forecasted values into original by applying the trend and seasonality constrains back to those that we previously separated.

## Trend
# The first step is to reduce the trend using transformation, as we can see here that there is a strong positive trend. These transformation can be log, sq-rt, cube root etc . Basically it penalizes larger values more than the smaller. In this case we will use the logarithmic transformation

import numpy as np
ts_log = np.log(ts)
plt.plot(ts_log)


# There is some noise in realizing the forward trend here. There are some methods to model these trends and then remove them from the series. Some of the common ones are:
# • Smoothing: using rolling/moving average
# • Aggression: by taking the mean for a certain time period (year/month)

# Smoothing:
# In smoothing we usually take the past few instances (rolling estimates) We will discuss two methods under smoothing- Moving average and Exponentially weighted moving average.

# Moving average - First take x consecutive values and this depends on the frequency if it is 1 year we take 12 values. Lucky for us that Pandas has a function for rolling estimate (“alright alright alright” -Matthew McConaughey!)
moving_avg = pd.DataFrame.rolling(ts_log, 12).mean()
def func():

    ## plotting timeseries log and timeseries log moving avg
    plt.plot(ts_log)
    plt.plot(moving_avg, color = 'red')

func()

# Removing Noise through smoothing
# Now subtract the rolling mean from the original series
ts_moving_avg_diff = ts - moving_avg
ts_log_moving_avg_diff = ts_log - moving_avg

ts_moving_avg_diff.head(15)
ts_log_moving_avg_diff.head(15)

test_stationarity(ts_moving_avg_diff)
test_stationarity(ts_log_moving_avg_diff)

# We notice two things:
# • The rolling values are varying slightly but there is no specific trend.
# • The test statistics is smaller than the 5 % critical values. That tells us that we are 95% confident that this series is stationary.


# In this example we can easily take a time period (12 months for a year), but there are situations where the time period range is more complex like stock price etc. So we use the exponentially weighted moving average (there are other weighted moving averages but for starters, lets use this). The previous values are assigned with a decay factor. Pandas again comes to the rescue with some awesome functions for it, like:

expwma_avg = pd.DataFrame.ewm(ts_log, halflife = 12)

def func():
    plt.plot(ts_log)
    plt.plot(expwm_avg.mean(), color = 'red', alpha = 0.3)

func()

ts_log_ewma_diff = ts_log - expwma_avg.mean()
test_stationarity(ts_log_ewma_diff)

# It is stationary because:
# • Rolling values have less variations in mean and standard deviation in magnitude.
# • the test statistic is smaller than 1% of the critical value. So we can say we are almost 99% confident that this is stationary.


# Seasonality (along with Trend)
# Previously we saw just trend part of the time series, now we will see both trend and seasonality. Most Time series have trends along with seasonality. There are two common methods to remove trend and seasonality, they are:
# • Differencing: by taking difference using time lag
# • Decomposition: model both trend and seasonality, then remove them

# Differencing:
# Here we first take the difference of the value at a particular time with that of the previous time. Now let’s do it in Pandas.

# Take first difference
ts_log_diff = ts_log - ts_log.shift()

test_stationarity(ts_log_diff)


# It is stationary because:
# • the mean and std variations have small variations with time.
# • test statistic is less than 10% of the critical values, so we can be 90 % confident that this is stationary.
#
# Decomposing:
# Here we model both the trend and the seasonality, then the remaining part of the time series is returned. Guess what? Yup, we have some awesome function for it. Let’s check it out:

from statsmodels.tsa.seasonal import seasonal_decompose
decomposition = seasonal_decompose(ts_log)

type(decomposition)
trend = decomposition.trend
seasonal = decomposition.seasonal
residual = decomposition.resid

def func():
    plt.subplot(411)
    plt.plot(ts_log, label = "Original")
    plt.legend(loc='best')
    plt.subplot(412)
    plt.plot(trend, label = "Trend")
    plt.legend(loc='best')
    plt.subplot(413)
    plt.plot(seasonal, label = "Seasonality")
    plt.legend(loc='best')
    plt.subplot(414)
    plt.plot(residual, label = "Residuals")
    plt.legend(loc='best')
    plt.tight_layout()

func()

# Remove the trend and seasonality from the Time series and now we can use the residual values. Let’s check stationarity.
ts_log_decombose = residual
test_stationarity(ts_log_decombose)

# This is stationary because:
# • test statistic is lower than 1% critical values.
# • the mean and std variations have small variations with time.


# Forecasting a Time Series
# Now that we have made the Time series stationary, let’s make models on the time series using differencing because it is easy to add the error , trend and seasonality back into predicted values .


# Auto Regressive Integrated Moving Average(ARIMA) — It is like a liner regression equation where the predictors depend on parameters (p,d,q) of the ARIMA model .
# Let me explain these dependent parameters:
# • p : This is the number of AR (Auto-Regressive) terms . Example — if p is 3 the predictor for y(t) will be y(t-1),y(t-2),y(t-3).
# • q : This is the number of MA (Moving-Average) terms . Example — if p is 3 the predictor for y(t) will be y(t-1),y(t-2),y(t-3).
# • d :This is the number of differences or the number of non-seasonal differences .

Now let’s check out on how we can figure out what value of p and q to use. We use two popular plotting techniques; they are:
• Autocorrelation Function (ACF): It just measures the correlation between two consecutive (lagged version). example at lag 4, ACF will compare series at time instance t1…t2 with series at instance t1–4…t2–4
• Partial Autocorrelation Function (PACF): is used to measure the degree of association between y(t) and y(t-p).

from statsmodels.tsa.arima_model import ARIMA, ARMA
# ACF & PACF plots
from statsmodels.tsa.stattools import acf, pacf

lag_acf = acf(ts_log_diff, nlags=20)
lag_pacf = pacf(ts_log_diff, nlags=20, method = 'ols')


def Corr():
    # Plot ACF
    plt.subplot(121)
    plt.plot(lag_acf)
    plt.axhline(y=0, linestyle = '--', color = 'gray')
    plt.axhline(y=-1.96/np.sqrt(len(ts_log_diff)), linestyle = '--', color = 'gray')
    plt.axhline(y=1.96/np.sqrt(len(ts_log_diff)), linestyle = '--', color = 'gray')
    plt.title('Autocorrelation Function')

    # Plot PACF
    plt.subplot(122)
    plt.plot(lag_pacf)
    plt.axhline(y=0, linestyle = '--', color = 'gray')
    plt.axhline(y=-1.96/np.sqrt(len(ts_log_diff)), linestyle = '--', color = 'gray')
    plt.axhline(y=1.96/np.sqrt(len(ts_log_diff)), linestyle = '--', color = 'gray')
    plt.title('Partial Autocorrelation Function')
    plt.tight_layout()

Corr()

# The two dotted lines on either sides of 0 are the confidence intervals. These can be used to determine the ‘p’ and ‘q’ values as:
# • p : This is the number of AR (Auto-Regressive) terms . Example — if p is 3 the predictor for y(t) will be y(t-1),y(t-2),y(t-3).
# • q : This is the number of MA (Moving-Average) terms . Example — if p is 3 the predictor for y(t) will be y(t-1),y(t-2),y(t-3).
# • d :This is the number of differences or the number of non-seasonal differences .

# • p: The first time where the PACF crosses the upper confidence interval, here its close to 2. hence p = 2.
# • q: The first time where the ACF crosses the upper confidence interval, here its close to 2. hence p = 2.

def plot_ar():
    # AR Model
    model = ARIMA(ts_log, order = (2,1,0))
    results_AR = model.fit(disp=-1)
    plt.plot(ts_log_diff)
    plt.plot(results_ARIMA.fittedvalues, color = 'red')
    plt.title('RSS: {0:.4f}'.format(sum((results_ARIMA.fittedvalues - ts_log_diff)**2)) )

plot_ar()

def plot_ma():
    # MA Model
    model = ARIMA(ts_log, order = (0, 1, 2))
    results_AR = model.fit(disp=-1)
    plt.plot(ts_log_diff)
    plt.plot(results_ARIMA.fittedvalues, color = 'red')
    plt.title('RSS: {0:.4f}'.format(sum((results_ARIMA.fittedvalues - ts_log_diff)**2)) )

plot_ar()

def plot_arma():
    # ARMA Model
    model = ARMA(ts_log, order = (2,1,0))
    results_AR = model.fit(disp=-1)
    plt.plot(ts_log_diff)
    plt.plot(results_ARIMA.fittedvalues, color = 'red')
    plt.title('RSS: {0:.4f}'.format(sum((results_ARIMA.fittedvalues - ts_log_diff)**2)) )

plot_arma()

def plot_arima():
    # ARIMA Model
    model = ARIMA(ts_log, order = (2,1,2))
    results_ARIMA = model.fit(disp=-1)
    plt.plot(ts_log_diff)
    plt.plot(results_ARIMA.fittedvalues, color = 'red')
    plt.title('RSS: {0:.4f}'.format(sum((results_ARIMA.fittedvalues - ts_log_diff)**2)) )

plot_arima()




# RSS values:
# • AR=1.5023
# • MA=1.472
# • ARIMA =1.0292
# ARIMA has the best RSS values.


##FINAL STEP: BRINGING THIS BACK TO THE ORIGINAL SCALE
# Steps involved:
# • First get the predicted values and store it as series. You will notice the first month is missing because we took a lag of 1(shift).
# • Now convert differencing to log scale: find the cumulative sum and add it to a new series with a base value (here the first-month value of the log series).

predications_ARIMA_diff = pd.Series(results_ARIMA.fittedvalues, copy = True)
print(predications_ARIMA_diff)

predications_ARIMA_diff_cumsum = predications_ARIMA_diff.cumsum()
predications_ARIMA_log = pd.Series(ts_log[0], index=ts_log.index)
predications_ARIMA_log = predications_ARIMA_log.add(predications_ARIMA_diff_cumsum, fill_value = 0)
# Next -take the exponent of the series from above (anti-log) which will be the predicted value — the time series forecast model.
# Now plot the predicted values with the original.
# • Find the RMSE

predications_ARIMA = np.exp(predications_ARIMA_log)

def func():
    plt.plot(ts, color = 'orange')
    plt.plot(predications_ARIMA, color = 'blue')
    plt.title('RSME: {0:.4f}'.format( np.sqrt(sum((predications_ARIMA-ts)**2 )/len(ts))) )

func()
