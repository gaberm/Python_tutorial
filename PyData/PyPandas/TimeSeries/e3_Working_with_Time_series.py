"""PythonDataScienceHandbook
https://jakevdp.github.io/PythonDataScienceHandbook/03.11-working-with-time-series.html
"""

import pandas as pd
import numpy as np
date = pd.to_datetime("4th of July, 2015")
date

date.strftime('%A')
date + pd.to_timedelta(np.arange(12), 'D')

index = pd.DatetimeIndex(['2014-07-04', '2014-08-04',
                          '2015-07-04', '2015-08-04'])
data = pd.Series([0, 1, 2, 3], index=index)
data

data['2014-07-04':'2015-07-04']
data['2014-08']
data['2015-08']
data['2015']

import datetime
datetime.datetime(2015, 7, 3)
dates = pd.to_datetime([datetime.datetime(2015, 7, 3), '4th of July, 2015', '2015-Jul-6', '07-07-2015', '20150708'])
dates

#Convert the curret datetime to a frequency within the same range
dates.to_period('D')
dates.to_period('M')


#Creating time range
pd.date_range('2015-07-03', '2015-07-10')

pd.date_range('2015-07-01', periods=8 )

pd.date_range('2015-07-03', periods=8, freq='H')

#And a sequence of durations increasing by an hour:
pd.timedelta_range(0, periods=10, freq='H')

pd.timedelta_range(0, periods=9, freq="2H30T")

# All of these short codes refer to specific instances of Pandas time series offsets, which can be found in the pd.tseries.offsets module. For example, we can create a business day offset directly as follows:
from pandas.tseries.offsets import BDay
pd.date_range('2015-07-01', periods=5, freq=BDay())


## ## Resampling, Shifting, and Windowing
from pandas_datareader import data


# api_key = open('C:\\Users\\Mohammed\\gitlab\\quandlapikey.txt','r').read()
# df = quandl.get("FMAC/HPI_TX", authtoken=api_key)
# df = data.DataReader("BAC", "quandl",start='2004', end='2016', access_key=api_key)

import pandas as pd
import matplotlib.pyplot as plt
import seaborn; seaborn.set()


BAC = data.DataReader("BAC", "iex", start='2013', end='2018')


BAC['close'].plot()
BAC.plot()

## Resampling and converting frequencies
# One common need for time series data is resampling at a higher or lower frequency. This can be done using the resample() method, or the much simpler asfreq() method.

goog = data.DataReader("GOOG", "iex", start='2013', end='2018')
goog.index = pd.to_datetime(goog.index)

goog = goog['close']
goog.plot();

goog.plot(alpha=0.5, style='-')
goog.resample('BA').mean().plot(style=':')
goog.asfreq('BA').plot(style='--');
plt.legend(['input', 'resample', 'asfreq'],
           loc='upper left');


fig, ax = plt.subplots(2, sharex=True)
data = goog.iloc[:10]

data.asfreq('D').plot(ax=ax[0], marker='o')

data.asfreq('D', method='bfill').plot(ax=ax[1], style='-o')
data.asfreq('D', method='ffill').plot(ax=ax[1], style='--o')
ax[1].legend(["back-fill", "forward-fill"]);

data.plot()



fig, ax = plt.subplots(3, sharey=True)

# apply a frequency to the data
goog = goog.asfreq('D', method='pad')

goog.plot(ax=ax[0])
goog.shift(900).plot(ax=ax[1])
goog.tshift(900).plot(ax=ax[2])

# legends and annotations
local_max = pd.to_datetime('2007-11-05')
offset = pd.Timedelta(900, 'D')

ax[0].legend(['input'], loc=2)
ax[0].get_xticklabels()[2].set(weight='heavy', color='red')
ax[0].axvline(local_max, alpha=0.3, color='red')

ax[1].legend(['shift(900)'], loc=2)
ax[1].get_xticklabels()[2].set(weight='heavy', color='red')
ax[1].axvline(local_max + offset, alpha=0.3, color='red')

ax[2].legend(['tshift(900)'], loc=2)
ax[2].get_xticklabels()[1].set(weight='heavy', color='red')
ax[2].axvline(local_max + offset, alpha=0.3, color='red');

goog.plot()
plt.figure()


ROI = 100 * (goog.tshift(-365) / goog - 1)
ROI.plot()
plt.ylabel('% Return on Investment');
