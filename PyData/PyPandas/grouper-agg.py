## Source:
# http://pbpython.com/pandas-grouper-agg.html
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


datafile = '/mnt/store/home/GitLibrary/Insights/input/20190115/A2P_Report_20190116_031311_A2P_Report.csv'

##load the CSV file
# dataset = pd.read_csv(datafile,  index_col='TimeStamp',parse_dates=['TimeStamp'], infer_datetime_format=True, engine='c')       #Dec19 - Added engine 'c'
dataset = pd.read_csv(datafile,  index_col=None, parse_dates=['TimeStamp'], infer_datetime_format=True, engine='c')       #Dec19 - Added engine 'c'
dataset.rename(columns={"TimeStamp":'Time',"FAF Applied Rule": 'FAF_rule',"FAF Filter": 'FAF_Filter', "SMS Content":'Message', "Data Coding Scheme":'dCoding'}, inplace=True )
dataset.dropna(inplace=True)

df = dataset[0:10000]
df.set_index('Time').resample('min')["Message"].count()

df.set_index('Time').groupby('Sender')["Message"].resample("min").sum()
df.groupby(['Sender', pd.Grouper(key='Time', freq='min')])['Message'].sum()
df.groupby(['Sender', pd.Grouper(key='Time', freq='min')])['Message'].count()
df.groupby(['Sender', pd.Grouper(key='Time')])['Message'].sum()
df.groupby(['Sender', pd.Grouper(key='Time', freq='A-DEC')])['Message'].count().sort_values( ascending = False)

X = dataset.groupby(['Sender', pd.Grouper(key='Time', freq='A-DEC')])['Message'].count().sort_values( ascending = False)
X = df.groupby(['Sender', pd.Grouper(key='Time', freq='A-DEC')])['Message'].count().sort_values( ascending = False)
X.head()
Y = X.reset_index()
# Y= X.unstack()
Y.Message.sum()

cutoff = .1 * Y.Message.sum()
Y.shape
[k,v if v > cutoff else 'value' for k,v in enumerate(X)]
Y.shape
Y.head()
for i in range(len(Y)):
    print(Y.Sender, Y.Message)


[Y.Message[k] for k in range(len(Y))]
[Y['Message'][k] for k in range(len(Y))]
[Y.Message[k] if condition else value for value in variable]
[Y[k].loc['Sender', 'Message'] for k in range(len(Y))]
Y.shape
Y.describe
import sklearn.datasets
data = sklearn.datasets.load_boston()
data.target.shape
data
df = data.data
dir(sklearn.datasets)
