### Introduction
# plotnine is a data visualization library which implements the grammar of graphics. The grammar of graphics is an approach to data visualization API design which diverges in the extreme from that followed by the libraries we have seen so far.

import pandas as pd
reviews = pd.read_csv(r"https://github.com/davestroud/Wine/raw/master/winemag-data-130k-v2.csv", index_col=0)
reviews.head()

# The Data element is a call to ggplot, which populates the data in the graph. The Aesthetics are controlled by the aes function, which populates our visual variables: colors, shapes, and so on. Finally, Layers are functions that add to or modify the plot itself.

# A plotnine plot consists of functions of these three types concatenated together with a plus (+) operator. The result is an extremely expressive way of building your charts!
# Let's jump into plotnine and see this grammar of graphics in action.

from plotnine import *

top_wines = reviews[reviews['variety'].isin(reviews['variety'].value_counts().head(5).index)]
top_wines.variety.unique()

df = top_wines.head(1000).dropna()

# Plotting by defining:
    # 1. data in ggplot
    # 2. Aestetics: our data params
    # 3. geom_point, the actual func like graph

# plotting a scatter
(ggplot(df)
 + aes('points', 'price')
 + geom_point())

# To keep changing the plot, just keep adding things. You can add a regression line with a stat_smooth layer:
df = top_wines.head(1000).dropna()

(    ggplot(df)
        + aes('points', 'price')
        + geom_point()
        + stat_smooth() )

# Adding colors
(    ggplot(df)
        + aes('points', 'price')
        + aes(color='points')
        + geom_point()
        + stat_smooth() )

(   ggplot(df)
        + aes('points', 'price')
        + aes(color='price')
        + geom_point()
        + stat_smooth()
        + facet_wrap('~country') )


# To apply faceting, use facet_wrap.

df = top_wines.head(1000).dropna()

(ggplot(df)
     + aes('points', 'price')
     + aes(color='points')
     + geom_point()
     + stat_smooth()
     + facet_wrap('~province'))


(ggplot(df)
     + aes('points', 'price')
     + aes(color='points')
     + geom_point()
     + stat_unique()
     + facet_wrap('~variety')
)

(ggplot(df)
 + geom_point(aes('points', 'price'))
 + stat_smooth(aes('points'))
)


(ggplot(top_wines)
     + aes('points')
     + geom_bar()
)

(ggplot(top_wines)
     + aes('points', 'variety')
     + geom_bin2d(bins=20)
)


(ggplot(top_wines)
         + aes('points', 'variety')
         + geom_bin2d(bins=20)
         + coord_fixed(ratio=1)
         + ggtitle("Top Five Most Common Wine Variety Points Awarded")
)


## Exercise:
pokemon = pd.read_csv(r"https://gist.githubusercontent.com/armgilles/194bcff35001e7eb53a2a8b441e8b2c6/raw/92200bc0a673d5ce2110aaad4544ed6c4010f687/pokemon.csv")
pokemon.columns

( ggplot(pokemon)
+ aes('Attack', 'Defense' )
+ geom_point()
+ ggtitle("Pokemon Attack"))

( ggplot(pokemon)
+ aes('Attack', 'Defense' )
+ aes(color='Legendary')
+ geom_point()
+ ggtitle("Pokemon Attack/Defense by Legendary"))

( ggplot(pokemon)
+ aes('Attack' )
+ aes(color='Legendary')
+ geom_histogram(bins = 24)
+ facet_wrap('~Generation')
+ ggtitle("Pokemon Attack/Generation Histogram"))
