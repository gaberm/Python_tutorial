# We're using lists to record people who attended our party and what order they arrived in. For example, the following list represents a party with 7 guests, in which Adela showed up first and Ford was the last to arrive:
#
#     party_attendees = ['Adela', 'Fleda', 'Owen', 'May', 'Mona', 'Gilbert', 'Ford']
#
# A guest is considered 'fashionably late' if they arrived after at least half of the party's guests. However, they must not be the very last guest (that's taking it too far). In the above example, Mona and Gilbert are the only guests who were fashionably late.
#
# Complete the function below which takes a list of party attendees as well as a person, and tells us whether that person is fashionably late.


def fashionably_late(arrivals, name):
    """Given an ordered list of arrivals to the party and a name, return whether the guest with that
    name was fashionably late.
    """
    half = len(arrivals)/2
    arrived = arrivals.index(name) + 1
    print('half', half, 'and arrived:', arrived )

    if arrived > half :
        if name == arrivals[-1]:
            return False
        return True
    else:
        return False


arrivals=['Adela', 'Fleda', 'Owen', 'May', 'Mona', 'Gilbert', 'Ford', 'Ella']; name='Adela'
arrivals=['Adela', 'Fleda', 'Owen', 'May', 'Mona', 'Gilbert', 'Ford']
fashionably_late(arrivals, 'May')


def menu_is_boring(meals):
    """Given a list of meals served over some period of time, return True if the
    same meal has ever been served two days in a row, and False otherwise.
    """

    [True if meals(x)==meals(x+1) for x in meals]

meals = ['egg', 'ham', 'turkey', 'spam']
meals = ['egg', 'ham', 'turkey','turkey', 'spam']
meals = []
menu_is_boring(l)

[True if meals[x]==meals[x+1] else False for x in range(len(meals)-1)]
any( [True if meals[x]==meals[x+1] else False for x in range(len(meals)-1)] )
[value if condition else value for value in variable]

("12345".isdigit() and  len("12345") ==5 )

help (str)








# A researcher has gathered thousands of news articles. But she wants to focus her attention on articles including a specific word. Complete the function below to help her filter her list of articles.
#
# Your function should meet the following criteria
#
# - Do not include documents where the keyword string shows up only as a part of a larger word. For example, if she were looking for the keyword “closed”, you would not include the string “enclosed.”
# - She does not want you to distinguish upper case from lower case letters. So the phrase “Closed the case.” would be included when the keyword is “closed”
# - Do not let periods or commas affect what is matched. “It is closed.” would be included when the keyword is “closed”. But you can assume there are no other types of punctuation.


def word_search(doc_list, keyword):
    """
    Takes a list of documents (each document is a string) and a keyword.
    Returns list of the index values into the original list for all documents
    containing the keyword.

    Example:
    doc_list = ["The Learn Python Challenge Casino.", "They bought a car", "Casinoville"]
    >>> word_search(doc_list, 'casino')
    >>> [0]
    """
    pass


doc_list = ["The Learn Python Challenge Casino.", "They bought a car", "Casinoville"]
>>> word_search(doc_list, 'casino')

docs = [ i.lower().strip('.').split() for i in doc_list]
docs[0].index('casino')
docs[0][4].find('acasino')
[docs[i] for i in range(len(docs))]
[value if condition else value for value in variable]


for i, doc in enumerate(doc_list):
    tokens = doc.split()
    normalized = [token.rstrip('.,').lower() for token in tokens]

    if keyword.lower() in normalized:
        indices.append(i)



# Now the researcher wants to supply multiple keywords to search for. Complete the function below to help her.
#
# (You're encouraged to use the `word_search` function you just wrote when implementing this function. Reusing code in this way makes your programs more robust and readable - and it saves typing!)

def multi_word_search(doc_list, keywords):
    """
    Takes list of documents (each document is a string) and a list of keywords.
    Returns a dictionary where each key is a keyword, and the value is a list of indices
    (from doc_list) of the documents containing that keyword

    >>> doc_list = ["The Learn Python Challenge Casino.", "They bought a car and a casino", "Casinoville"]
    >>> keywords = ['casino', 'they']
    >>> multi_word_search(doc_list, keywords)
    {'casino': [0, 1], 'they': [1]}
    """
doc_list = ["The Learn Python Challenge Casino.", "They bought a car and a casino", "Casinoville"]
keywords = ['casino', 'they']

for i, doc in enumerate(doc_list):
    tokens = doc.split()
    normalized = [token.rstrip('.,').lower() for token in tokens]

    for j, keyword in enumerate(keywords):
        indices = []
        # print(j, keyword)
        # print( {keyword: j})
        if keyword.lower() in normalized:
            indices.append(i)
        results = {keyword: indices}

print(results)

keyword_tuple = {}
for keyword in keywords:
    keyword_tuple[keyword] = word_search(documents, keyword)



import numpy as np
print("numpy.random is a", type(np.random))
print("it contains names such as...",
      dir(np.random)[-15:]      )


# Roll 10 dice
rolls = np.random.randint(low=1, high=6, size=10)
rolls

rolls.mean()


[3, 4, 1, 2, 2, 1] + 10
rolls + 10

rolls.tolist()

# That "ravel" attribute sounds interesting. I'm a big classical music fan.
help(rolls.ravel)

# At which indices are the dice less than or equal to 3?
rolls <= 3

xlist = [[1,2,3],[2,4,6],]

# Create a 2-dimensional array
x = np.asarray(xlist)
print("xlist = {}\nx =\n{}".format(xlist, x))
type(x)
type(xlist)

x[1,-1]

print(dir(list))

############################################################################################


race =  [
        {'name': 'Peach', 'items': ['green shell', 'banana', 'green shell',], 'finish': 3},
        {'name': 'Bowser', 'items': ['green shell',], 'finish': 1},
        # Sometimes the racer's name wasn't recorded
        {'name': None, 'items': ['mushroom',], 'finish': 2},
        {'name': 'Toad', 'items': ['green shell', 'mushroom'], 'finish': 1},
    ]

race[2]['name']

import pandas as pd
import numpy as np
dt = [30, 21]
pd.DataFrame( [[30], [21]], columns=['Apples', 'Bananas'] )
pd.DataFrame( [dt], columns=['Apples', 'Bananas'] )


dt = {'Flour': '4 cups', 'Milk': '1 cup', 'Eggs': '2 large'}
type(dt)
ingredients = pd.Series(dt, index = dt.keys())

pd.read_csv
