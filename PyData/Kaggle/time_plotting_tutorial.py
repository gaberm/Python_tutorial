# In all of the sections thus far our visualizations have focused on and used numeric variables: either categorical variables, which fall into a set of buckets, or interval variables, which fall into an interval of values. In this notebook we will explore another type of variable: a time-series variable.

import pandas as pd
pd.set_option('max_columns', None)
import numpy as np


# stocks = pd.read_csv("../input/nyse/prices.csv", parse_dates=['date'])
df = pd.read_csv(r"https://github.com/szrlee/Stock-Time-Series-Analysis/raw/master/data/all_stocks_2017-01-01_to_2018-01-01.csv", parse_dates=['Date'] )
df.head()
stock = df[df['Name'] == "GOOGL"].set_index('Date')
stocks = df.set_index('Date')
stocks = df.copy().set_index('Date')
stock.head()
stocks.head()

# For daily data like this using a date like this is convenient. But a period can technically be for any length of time. pandas provides a whole dedicated type, the pandas.Period dtype (documented here), for this concept.

# In the "weak case", dates act as timestamps: they tell us something about when an observation occurred. For example, in the following dataset of animal shelter outcomes, there are two columns, datetime and date_of_birth, which describe facts about the animal in the observation.
help (pd.Period)

shelter = pd.read_csv(r"https://github.com/rdowns26/austin-animal-shelter/raw/master/Austin_Animal_Center_Outcomes.csv", parse_dates=['Date of Birth','DateTime'])

shelter = shelter.rename(columns = {'Animal ID': 'animal_id', 'Name': 'name', 'DateTime':'datetime', 'MonthYear':'monthyear', 'Date of Birth': 'date_of_birth', 'Outcome Type':'outcome_type', 'Outcome Subtype':'outcome_subtype', 'Animal Type': 'animal_type', 'Sex upon Outcome': 'sex_upon_outcome', 'Age upon Outcome': 'age_upon_outcome', 'Breed':'breed', 'Color': 'color'})
# import dateutil
# df['DateTime'] = df['DateTime'].apply(dateutil.parser.parse)
shelter.dtypes
df = shelter.copy()
shelter_outcomes = shelter[ [ 'datetime','outcome_type', 'age_upon_outcome', 'animal_type', 'breed', 'color', 'sex_upon_outcome', 'date_of_birth'] ]

shelter_outcomes.head()

shelter_outcomes['date_of_birth'].value_counts().sort_values().plot.line()

shelter_outcomes['date_of_birth'].value_counts().plot.line()

shelter_outcomes['date_of_birth'].value_counts().resample('Y').sum().plot.line()
shelter_outcomes['date_of_birth'].value_counts().resample('6M').sum().plot.line()


stocks.index = pd.to_datetime(stocks.Date)
stocks.dtypes

stocks['Volume'].resample('3M').mean().plot.bar()

stock['Volume'].resample('3M').mean().plot.bar()
stocks['Volume'].resample('3M').mean().plot.bar()

df.set_index('Date')['Volume'].resample('3M').mean().plot.bar()


## New Plot Types:
# Lag Plot
# One of these plot types is the lag plot. A lag plot compares data points from each observation in the dataset against data points from a previous observation. So for example, data from December 21st will be compared with data from December 20th, which will in turn be compared with data from December 19th, and so on. For example, here is what we see when we apply a lag plot to the volume (number of trades conducted) in the stock data:

import pandas as pd
from pandas.plotting import lag_plot

lag_plot(stock['Volume'].tail(250))
lag_plot(stocks['Volume'].tail(250))

# looks like days when volume is high are somewhat correlated with one another. A day of frantic trading does somewhat signal that the next day will also involve frantic trading.
# Time-series data tends to exhibit a behavior called periodicity: rises and peaks in the data that are correlated with time. For example, a gym would likely see an increase in attendance at the end of every workday, hence exhibiting a periodicity of a day. A bar would likely see a bump in sales on Friday, exhibiting periodicity over the course of a week. And so on.

# Lag plots are extremely useful because they are a simple way of checking datasets for this kind of periodicity.
# Note that they only work on "strong case" timeseries data.

### Autocorrelation plot
# A plot type that takes this concept and goes even further with it is the autocorrelation plot. The autocorrelation plot is a multivariate summarization-type plot that lets you check every periodicity at the same time. It does this by computing a summary statistic—the correlation score—across every possible lag in the dataset. This is known as autocorrelation.

# In an autocorrelation plot the lag is on the x-axis and the autocorrelation score is on the y-axis. The farther away the autocorrelation is from 0, the greater the influence that records that far away from each other exert on one another.

# Here is what an autocorrelation plot looks like when applied to the stock volume data:

from pandas.plotting import autocorrelation_plot
autocorrelation_plot ( stock['Volume'])
autocorrelation_plot ( stocks['Volume'])

# It seems like the volume of trading activity is weakly descendingly correlated with trading volume from the year prior. There aren't any significant non-random peaks in the dataset, so this is good evidence that there isn't much of a time-series pattern to the volume of trade activity over time.

# Of course, in this short optional section we're only scratching the surface of what you can do with do with time-series data. There's an entire literature around how to work with time-series variables that we are not discussing here. But these are the basics, and hopefully enough to get you started analyzing your own time-dependent data!



### Exercises:

crypto = pd.read_csv("https://github.com/JesseVent/Crypto-Market-Scraper/raw/master/crypto-markets.csv")

bitcoin = crypto [crypto['name'] == 'Bitcoin']
# bitcoin['date'] = pd.to_datetime(bitcoin['date'])
bitcoin.loc[:,'date']  = pd.to_datetime(bitcoin.loc[:,'date'])

bitcoin.head()


### Questions
# 1. Time-series data is really a special case of interval data.
# 2. Resampling is often useful in data visualization because it can help clean up and denoise our plots by aggregating on a different level.
# 3. Lag is the time-difference for each observation in the dataset. Autocorrelation is correlation applied to lag.

shelter_outcomes.head()
shelter_outcomes.resample('Y').plot()
shelter_outcomes['datetime'].plot()
shelter_outcomes['datetime'].value_counts().resample('Y').plot.line()
shelter_outcomes['datetime'].value_counts().resample('Y').count()
shelter_outcomes['datetime'].value_counts().resample('Y').count().plot.line()

crypto.tail()
lag_plot(crypto['volume'].tail(250))
lag_plot(bitcoin['volume'].tail(250))


autocorrelation_plot(crypto['volume'])
