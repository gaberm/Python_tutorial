## Introduction to plotly
# So far in this tutorial we have been using seaborn and pandas, two mature libraries designed around matplotlib. These libraries all focus on building "static" visualizations: visualizations that have no moving parts. In other words, all of the plots we've built thus far could appear in a dead-tree journal article.
#
# The web unlocks a lot of possibilities when it comes to interactivity and animations. There are a number of plotting libraries available which try to provide these features.
# In this section we will examine plotly, an open-source plotting library that's one of the most popular of these libraries.


import pandas as pd
reviews = pd.read_csv(r"https://github.com/davestroud/Wine/raw/master/winemag-data-130k-v2.csv", index_col=0)
reviews.head()

# plotly provides both online and offline modes. The latter injects the plotly source code directly into the notebook; the former does not. I recommend using plotly in offline mode the vast majority of the time, and it's the only mode that works on Kaggle (which disables network access in Python).

from plotly.offline import init_notebook_mode, iplot
init_notebook_mode(connected=True)

# We'll start by creating a basic scatter plot.
import plotly.graph_objs as go

# Performance penality for big data sets
iplot([go.Scatter(x=reviews.head(1000)['points'], y=reviews.head(1000)['price'], mode='markers')])


# Notice the "shape" of the plotly API. iplot takes a list of plot objects and composes them for you, plotting the combined end result. This makes it easy to stack plots.
#
# As another example, here's a KDE plot (what plotly refers to as a Histogram2dContour) and scatter plot of the same data.

iplot([go.Histogram2dContour(x=reviews.head(500)['points'],  y=reviews.head(500)['price'],                           contours=go.Contours(coloring='heatmap')),       go.Scatter(x=reviews.head(1000)['points'], y=reviews.head(1000)['price'], mode='markers')])


# plotly exposes several different APIs, ranging in complexity from low-level to high-level. iplot is the highest-level API, and hence, the most convenient one for general-purpose use.
#
# Personally I've always found the plotly Surface its most impressive feature (albeit one of the hardest to get right):
df = reviews.assign(n=0).groupby(['points', 'price']).count().reset_index()
df = reviews.assign(n=0).groupby(['points', 'price'])['n'].count().reset_index()

df.head()
reviews.head()
df = df[df["price"] < 100]
v = df.pivot(index='price', columns='points', values='n').fillna(0).values.tolist()

iplot([go.Surface(z=v)])

# plotly is often used to make choropleths. Choropleths are a type of map, where all of the entities (countries, US states, etc.) are colored according to some variable in the dataset. plotly is one of the most convenient plotting libraries available for making them.

df = reviews['country'].replace("US", "United States").value_counts()

iplot([go.Choropleth( locationmode='country names',
    locations=df.index.values,
    text=df.index,
    z=df.values )])

# Overall, plotly is a powerful, richly interactive data visualization library. It allows us to generate plots with more "pizazz" than standard pandas or seaborn output.


## Exercise:
pokemon = pd.read_csv(r"https://gist.githubusercontent.com/armgilles/194bcff35001e7eb53a2a8b441e8b2c6/raw/92200bc0a673d5ce2110aaad4544ed6c4010f687/pokemon.csv")

pokemon.head(5)

iplot( [go.Scatter(y = pokemon.Defense, x = pokemon.Attack, mode = 'markers', opacity=.7)])
