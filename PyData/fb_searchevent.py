""" Not WORKING"""
## https://towardsdatascience.com/how-to-use-facebook-graph-api-and-extract-data-using-python-1839e19d6999

import urllib3
import facebook
import requests

## Defingng App / Access Token
token= 'EAADrDL1RYJMBAFoxOZBUVgGKDq1A43ImvMM9Ql5HGpiFm2FH7s3i2q6qNb6vxP3HySpA47ZAtTPS1gOcqxA3mtslJK5ZA11q0YGT0Ifpn5NRJT2jl075OP9XwhSsrTfxMOHlYMnmpLeIl0fgTdeuub14VY2HhIZD'


##Getting list of Events:
#Now to find information on events for any search term say "Poetry" and limiting those events' number to 10000:

graph = facebook.GraphAPI(access_token=token, version = 2.12)
events = graph.request('/search?q=Metal&type=event&limit=10000')
eventList = events['data']


# Get the EventID of the first event in the list by
eventid = eventList[1]['id']

# For this EventID, get all information and set few variables which will be used later by:
event1 = graph.get_object(id=eventid, fields='attending_count,can_guests_invite,category,cover,declined_count,description,end_time,guest_list_enabled,interested_count,is_canceled,is_page_owned,is_viewer_admin,maybe_count,noreply_count,owner,parent_group,place,ticket_uri,timezone,type,updated_time')
attenderscount = event1['attending_count']
declinerscount = event1['declined_count']
interestedcount = event1['interested_count']
maybecount = event1['maybe_count']
noreplycount = event1['noreply_count']


# Getting the list of all those who are attending an event and converting the response into json format:
attenders = requests.get("https://graph.facebook.com/v2.8/"+eventid+"/attending?access_token="+token+"&limit="+str(attenderscount))
attenders_json = attenders.json()

# Getting the admins of the event:
admins = requests.get("https://graph.facebook.com/v2.8/"+eventid+"/admins?access_token="+token)
admins_json = admins.json()
