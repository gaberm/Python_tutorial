import pandas as pd
import datetime
#import pandas.io.data as web
#import pandas_datareader
from pandas_datareader import data, wb
import matplotlib as plt
from matplotlib import style
style.use('ggplot')

start = datetime.datetime(2010,1,1)
end = datetime.datetime(2019,1,1)

df = data.DataReader("XOM", "yahoo", start, end)
df = data.DataReader("AAPL", "yahoo", start, end)

print(df.head())
df['Adj Close'].plot()
