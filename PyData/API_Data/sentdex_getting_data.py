import matplotlib.pyplot as plt
import numpy as np
import urllib
import matplotlib.dates as mdates


def bytespdate2num(fmt, encoding = 'utf-8'):
    strconverter = mdates.strpdate2num(fmt)
    def bytesconverter(b):
        s = b.decode(encoding)
        return strconverter(s)
    return bytesconverter

def graph_data(stock):
    # stock_price_url = 'http://'
    stock_price_url = 'https://pythonprogramming.net/yahoo_finance_replacement'
    source_code = urllib.request.urlopen(stock_price_url).read().decode()

    stock_data = []
    split_source = source_code.split('\n')

    split_source
    # for line in split_source:
    #     split_line = line.split(',')
    #     if len(split_line == 6):
    #         if 'values' not in line:
    #             stock_data.append(line)

    # Date,Open,High,Low,Close,Adjusted_close,Volume

    date, openp, highp, lowp, closesp, adjclosesp, volume = np.loadtxt(split_source,
        delimiter=',',
        unpack=True,
        skiprows=1,
        # %Y = full year 2015
        # %y = partial year 15
        # %m = number month
        # %d = number day
        # %H = hours
        # %M = minutes
        # %S = seconds
        converters={0: bytespdate2num('%Y-%m-%d')})

    plt.plot_date(date, closesp, '-', label = 'Closing Price')
    plt.plot_date(date, adjclosesp, '--', label = 'Adjusted Close Price')
    plt.plot(date, closesp, label = 'Closing Price', linewidth = 1)
    plt.plot(date, adjclosesp, label = 'Adjusted Close Price', linewidth = 1)

    plt.xlabel('Date')
    plt.ylabel('Price')
    plt.title('APPL Stock')
    plt.legend()
    plt.show()


graph_data('AAPL')
