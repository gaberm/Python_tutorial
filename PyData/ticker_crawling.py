#https://medium.freecodecamp.org/how-to-scrape-websites-with-python-and-beautifulsoup-5946935d93fe
""" Stopped working with Blomberg """
# <meta itemprop="name" content="S&amp;P 500 Index">
# <meta itemprop="url" content="https://www.bloomberg.com/quote/SPX:IND">
# <meta itemprop="tickerSymbol" content="SPX">
# <meta itemprop="exchange">
# <meta itemprop="price" content="2737.13">
# <meta itemprop="priceChange" content="+6.93">
# <meta itemprop="priceChangePercent" content="+0.25%">
# <meta itemprop="quoteTime" content="2018-11-16T20:52:47.000Z">
# <meta itemprop="priceCurrency" content="USD">
#
# <div class="overviewRow__0956421f"><span class="priceText__1853e8a5">2,735.27</span><span class="currency__defc7184">USD</span></div><div class="overviewRow__0956421f"><span class="changeAbsolute__395487f7 positive__66b32664 animatedBackground__bc43aa59">+5.07</span><span class="changePercent__2d7dc0d2 positive__66b32664 animatedBackground__bc43aa59">+0.19%</span><span class="triangle__abc02a0b up__ada1f6eb null"></span></div>

# import libraries
# import urllib2
import urllib.request as urllib2
from bs4 import BeautifulSoup

# import urllib3
# specify the url
quote_page = 'http://www.bloomberg.com/quote/SPX:IND'

# query the website and return the html to the variable ‘page’
page = urllib2.urlopen(quote_page)

# parse the html using beautiful soup and store in variable `soup`
soup = BeautifulSoup(page, 'html.parser')

print(soup)
