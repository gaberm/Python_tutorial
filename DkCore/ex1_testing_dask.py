# https://towardsdatascience.com/trying-out-dask-dataframes-in-python-for-fast-data-analysis-in-parallel-aa960c18a915
# https://github.com/StrikingLoo/dask-dataframe-benchmarking

# !curl https://github.com/StrikingLoo/dask-dataframe-benchmarking/blob/master/random_people.csv

import dask.dataframe as ddf
import pandas as pd
from datetime import datetime

from multiprocessing import Pool
import multiprocessing
dir(multiprocessing)

dd = ddf.read_csv('random_people.csv')
df.head(10)
dd['bonus'] = dd['salary']*.5
df = pd.read_csv('random_people.csv')
df['bonus'] = df['salary']*.5

def benchmark(function, function_name):
    start = time.time()
    for _ in range(100):
        function()
    end = time.time()
    print("{0} millisecs for {1}".format((end - start)*1000, function_name))


def get_bonus(df):
    df['bonus'] = df['salary']*.5

def test_1():
    get_bonus(dd)
def test_2():
    get_bonus(df)

# benchmarking small frame and Pandas WON!!
benchmark(test_1, 'dask dataframe nuevo')
benchmark(test_2, 'pandas dataframe viejo')

df2 = pd.concat([df for _ in range(1000)])
df3 = pd.concat([df2 for _ in range(500)])
df3.shape

dfn = ddf.from_pandas(df3, npartitions=8)
dfn.shape


def test_big():
    get_bonus(dfn)
def test_big_old():
    get_bonus(df3)

def get_big_mean():
    return dfn.salary.mean().compute()
def get_big_mean_old():
    return df3.salary.mean()

def get_big_max():
    return dfn.salary.max().compute()
def get_big_max_old():
    return df3.salary.max()

def get_big_sum():
    return dfn.salary.sum().compute()
def get_big_sum_old():
    return df3.salary.sum()

def filter_df():
    df = dfn[dfn['salary']>5000]
def filter_df_old():
    df = df3[df3['salary']>5000]

def run_benchmarks():
    for i,f in enumerate([test_big, #test_big_old,
                          get_big_mean,# get_big_mean_old,
                          get_big_max, #get_big_max_old,
                          get_big_sum, #get_big_sum_old,
                          filter_df,#filter_df_old
                         ]):
        benchmark(f, f.__name__)



def f(x):
    return (13*x+5)%7

def apply_random_old():
    df3['random']= df3['salary'].apply(f)

def apply_random():
    dfn['random']= dfn['salary'].apply(f).compute()

def value_count_test():
    dfn.salary.value_counts().compute()

def value_count_test_old():
    df3.salary.value_counts()



run_benchmarks()
benchmark(apply_random, apply_random.__name__)
#benchmark(apply_random_old, apply_random_old.__name__)
benchmark(value_count_test, value_count_test.__name__)
#benchmark(value_count_test_old, value_count_test_old.__name__)


dfn.salary.value_counts().compute()
dfn.head(10)
