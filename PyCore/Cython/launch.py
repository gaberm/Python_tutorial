# launch.py - Python stub loader, loads the module that was made by Cython.

# This code is always interpreted, like normal Python.
# It is not compiled to C.

import hello
hello.say_hello()

%load_ext Cython
%%cython
def f(n):
   a = 0
   for i in range(n):
       a += i
   return a

cpdef g(int n):
   cdef int a = 0, i
   for i in range(n):
       a += i
   return a
