# hello.pyx - Python Module, this code will be translated to C by Cython.
def say_hello():
    print "Hello World!"

## setup.py - unnecessary if not redistributing the code, see below
#from setuptools import setup
#from Cython.Build import cythonize

#setup(name = 'Hello world app',
#      ext_modules = cythonize("*.pyx"))
