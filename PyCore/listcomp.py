""" TODO Turn to Cython Program """
import timeit
## Timing - How to Call
# t=timeit.Timer(stmt=Lookupfn, setup="pass")
# print('\nelapsed time: {0:.3f} when using LookUpFn.'.format(t.timeit(number=10)))
""" A Pythagorean triple consists of three positive integers a, b, and c, such that  a2 + b2 = c2.
Such a triple is commonly written (a, b, c), and the best known example is (3, 4, 5).
The following list comprehension creates the Pythagorean triples: """

def PythagX():
    [(x,y,z) for x in range(1,100) for y in range(x,100) for z in range(y,100) if x**2 + y**2 == z**2]


def PythagLoop():
    list=[]
    for x in range(1,100):
        for y in range(x,100):
            for z in range(y,100):
                if x**2 + y**2 == z**2 : list.append([x,y,z])
    #print(list)
t1=timeit.Timer(stmt=PythagX, setup="pass")
t2=timeit.Timer(stmt=PythagLoop, setup="pass")


# print('\nelapsed time: {0:.3f} when using List comperhansion.'.format(t1.timeit(number=10)))
# print('\nelapsed time: {0:.3f} when using a Loop.'.format(t2.timeit(number=10)))

""" Another example: Let A and B be two sets, the cross product (or Cartesian product) of A and B, written A×B,
is the set of all pairs wherein the first element is a member of the set A and the second element is a member of the set B.
Mathematical definition:
A×B = {(a, b) : a belongs to A, b belongs to B}.
It's easy to be accomplished in Python: """

colours = [ "red", "green", "yellow", "blue" ]
things = [ "house", "car", "tree" ]
coloured_things = [ (x,y) for x in colours for y in things ]
print(coloured_things)
[('red', 'house'), ('red', 'car'), ('red', 'tree'), ('green', 'house'), ('green', 'car'), ('green', 'tree'), ('yellow', 'house'), ('yellow', 'car'), ('yellow', 'tree'), ('blue', 'house'), ('blue', 'car'), ('blue', 'tree')]

x = (x **2 for x in range(20))
print(list(x))

# Calculation of the prime numbers between 1 and 100 using the sieve of Eratosthenes:
noprimes = [j for i in range(2, 8) for j in range(i*2, 100, i)]
print(noprimes)

print(list(x for x in range(2,8)))
print(list(x for x in range(2,8)))
primes = [x for x in range(2, 100) if x not in noprimes]
print(primes)
[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]

""" Set Comprehensions """
from math import sqrt
n = 100
sqrt_n = int(sqrt(n))
print(sqrt_n)
no_primes = {j for i in range(2, sqrt_n+1) for j in range(i*2, n, i)}
no_primes
{4, 6, 8, 9, 10, 12, 14, 15, 16, 18, 20, 21, 22, 24, 25, 26, 27, 28, 30, 32, 33, 34, 35, 36, 38, 39, 40, 42, 44, 45, 46, 48, 49, 50, 51, 52, 54, 55, 56, 57, 58, 60, 62, 63, 64, 65, 66, 68, 69, 70, 72, 74, 75, 76, 77, 78, 80, 81, 82, 84, 85, 86, 87, 88, 90, 91, 92, 93, 94, 95, 96, 98, 99}
primes = {i for i in range(2, n) if i not in no_primes}
print(primes)
{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97}

from math import sqrt
def primes(n):
    if n == 0:
        return []
    elif n == 1:
        return []
    else:
        p = primes(int(sqrt(n)))
        no_p = {j for i in p for j in range(i*2, n+1, i)}
        p = {x for x in range(2, n + 1) if x not in no_p}
    return p

for i in range(1,50):
    print(i, primes(i))


primes(38)

x = "Python 3 fixed the dirty little secret"
res = [t for t in range(3)]
print(res)
x
