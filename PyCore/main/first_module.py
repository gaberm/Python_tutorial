print("This will always run.")
import sys, os, time, optparse

def main(x):
    print("First module's Main Method")
    NoneDef(x)
def NoneDef(x):

    print(f'This is there but not defined {x}\t')

if __name__ == '__main__':
    now = time.time()
    main(now)
    print("Run Directly")
