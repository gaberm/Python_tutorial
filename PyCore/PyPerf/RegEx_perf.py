# s.translate(None, string.punctuation)

import re, string, timeit

s = "string. With. Punctuation"
exclude = set(string.punctuation)
table = str.maketrans("","")
regex = re.compile('[%s]' % re.escape(string.punctuation))

def test_set(s):
    return ''.join(ch for ch in s if ch not in exclude)

def test_re(s):  # From Vinko's solution, with fix.
    return regex.sub('', s)

def test_trans(s):
    # return s.translate(table, string.punctuation)
    return s.translate(string.punctuation)

def test_repl(s):  # From S.Lott's solution
    for c in string.punctuation:
        s=s.replace(c,"")
    return s

print ("sets      :",timeit.Timer('f(s)', 'from __main__ import s,test_set as f').timeit(1000000))
print( "regex     :",timeit.Timer('f(s)', 'from __main__ import s,test_re as f').timeit(1000000))
print ("translate :",timeit.Timer('f(s)', 'from __main__ import s,test_trans as f').timeit(1000000))
print ("replace   :",timeit.Timer('f(s)', 'from __main__ import s,test_repl as f').timeit(1000000))
# t1 = timeit.Timer( stmt=StrRep, setup="pass")
# print('\nelapsed time: {0:.3f} when using RE Sub.'.format(t1.timeit(number=10)))
# print ("sets      :",timeit.Timer('f(s)', 'from __main__ import s,Function as f').timeit(1000000))
### elapsed time: 0.110 when using Str Replace.
# string.maketrans
(0.1+0.2)-0.3
