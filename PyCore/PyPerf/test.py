#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Libraries import!!
# import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
# # Trying Fuzzy Wuzzy
from fuzzywuzzy import fuzz # fuzz is used to compare TWO strings
from fuzzywuzzy import process # process is used to compare a string to MULTIPLE other strings
from tabulate import tabulate # Used for Charts graphical representation
# import re   # RegEX
import timeit # Timing module for Perf measurment
#Environment defintion!!
pd.options.mode.chained_assignment = None  # default='warn'

results=[]

#Loading Source
#print(open('.\input\input_small.csv').read())
# def LoadTable():
#     table = pd.read_table('.\input\input60k.csv', sep=',', chunksize=20 )
#     return table
#     for chunk in table:
#         print(chunk)
#
# LoadTable()

table = pd.read_table('.\input\input60k.csv', sep=',', chunksize=20 )
# for chunk in table:
#     print(chunk)

print(table.get_chunk(200))
# df=msgdata
# print(df.head(10))
#
#datafile=LoadTable()

#print(tabulate(datafile, tablefmt='psql'))


# t=timeit.Timer(stmt=LoadTable, setup="pass")
# print('\nelapsed time: {0:.3f} when using open fun.'.format(t.timeit(number=10)))
