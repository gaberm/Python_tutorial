import cProfile, pstats, io

#Call Method
# from PyProfiling import profile as PyProfile
# @PyProfile
def profile(fnc):

    """A decorator that uses cProfile to profile a function"""

    def inner(*args, **kwargs):

        pr = cProfile.Profile()
        pr.enable()
        retval = fnc(*args, **kwargs)
        pr.disable()
        s = io.StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        print(s.getvalue())
        return retval

    return inner

# Our Function ..
def read_movies(src):

    with open(src) as fd:
        return fd.read().splitlines()

def is_duplicate(needle, haystack):
    for movie in haystack:
        if needle.lower() == movie.lower():
            return True
    return False

@profile
def find_duplicate_movies(src='/mnt/store/home/GitLibrary/Insights/input/input_small.csv'):

    movies = read_movies(src)
    duplicates = []
    while movies:
        movie = movies.pop()
        if is_duplicate(movie, movies):
            duplicates.append(movie)
    return duplicates



# @profile
# def find_duplicate_movies(src='/mnt/store/home/GitLibrary/Insights/input/input_small.csv'):
#
#     movies = read_movies(src)
#     movies = [movie.lower() for movie in movies]
#     movies.sort()
#     duplicates = [movie1 for movie1, movie2 in zip(movies[:-1], movies[1:]) if movie1 == movie2]
#     return duplicates
#

find_duplicate_movies()
