from __future__ import print_function
import timeit

t1=timeit.Timer('Counter(l)', \
                'import random;import string;from collections import Counter;n=1000;l=[random.choice(string.ascii_letters) for x in range(n)]'
                )

t2=timeit.Timer('[[x,l.count(x)] for x in set(l)]',
                'import random;import string;n=1000;l=[random.choice(string.ascii_letters) for x in range(n)]'
                )

# print("Counter(): ", t1.repeat(repeat=3,number=10000))
# print("count():   ", t2.repeat(repeat=3,number=10000))

# print("Counter(): ", t1.timeit(number=10000))
# print("count():   ", t2.timeit(number=10000))

# timeit.default_timer()
print('\nelapsed time: {0:.3f} when using Counter().'.format(t1.timeit(number=100)))
print('\nelapsed time: {0:.3f} when using Count() Loop.'.format(t2.timeit(number=100)))
