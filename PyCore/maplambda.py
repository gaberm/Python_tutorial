""" Lambda Function """
sum = lambda x, y : x + y
sum(1,1)
seq=(1,1)
# r = map(func, seq)
# print(r)

def fahrenheit(T):
    return ((float(9)/5)*T + 32)

def celsius(T):
    return (float(5)/9)*(T-32)


""" MAP Function """

temperatures = (36.5, 37, 37.5, 38, 39)
F = map(fahrenheit, temperatures)
C = map(celsius, F)
print(F)
# <map object at 0x0000026BD2095E48>

temperatures_in_Fahrenheit = list(map(fahrenheit, temperatures))
temperatures_in_Celsius = list(map(celsius, temperatures_in_Fahrenheit))
print(temperatures_in_Fahrenheit)
# [97.7, 98.60000000000001, 99.5, 100.4, 102.2]
print(temperatures_in_Celsius)
# [36.5, 37.00000000000001, 37.5, 38.00000000000001, 39.0]

C = [39.2, 36.5, 37.3, 38, 37.8]
F = list(map(lambda x: (float(9)/5)*x + 32, C))
print(F)
[102.56, 97.7, 99.14, 100.4, 100.03999999999999]
C = list(map(lambda x: (float(5)/9)*(x-32), F))
print(C)
[39.2, 36.5, 37.300000000000004, 38.00000000000001, 37.8]

a = [1, 2, 3, 4]
b = [17, 12, 11, 10]
c = [-1, -4, 5, 9]
list(map(lambda x, y : x+y, a, b))
[18, 14, 14, 14]
list(map(lambda x, y, z : x+y+z, a, b, c))
[17, 10, 19, 23]
list(map(lambda x, y, z : 2.5*x + 2*y - z, a, b, c))
[37.5, 33.0, 24.5, 21.0]

a = [1, 2, 3]
b = [17, 12, 11, 10]
c = [-1, -4, 5, 9]

list(map(lambda x, y, z : 2.5*x + 2*y - z, a, b, c))
[37.5, 33.0, 24.5]

from math import sin, cos, tan, pi

def map_functions(x, functions):
     """ map an iterable of functions on the the object x """
     res = []
     for func in functions:
         res.append(func(x))
     return res

family_of_functions = (sin, cos, tan)
print(map_functions(pi, family_of_functions))

def map_functions(x, functions):
     return [ func(x) for func in functions ]



"""Filtering:"""
# filter(function, sequence)
# offers an elegant way to filter out all the elements of a sequence "sequence",
# for which the function function returns True.
fibonacci = [0,1,1,2,3,5,8,13,21,34,55]
odd_numbers = list(filter(lambda x: x % 2, fibonacci))
print(odd_numbers)
[1, 1, 3, 5, 13, 21, 55]
even_numbers = list(filter(lambda x: x % 2 == 0, fibonacci))
print(even_numbers)
[0, 2, 8, 34]

# or alternatively:
even_numbers = list(filter(lambda x: x % 2 -1, fibonacci))
print(even_numbers)
[0, 2, 8, 34]

""" Reducing """
# reduce(func, seq)
# continually applies the function func() to the sequence seq. It returns a single value.

#If seq = [ s1, s2, s3, ... , sn ], calling reduce(func, seq) works like this:
# - At first the first two elements of seq will be applied to func, i.e. func(s1,s2) The list on which reduce() works looks now like this: [ func(s1, s2), s3, ... , sn ]
# - In the next step func will be applied on the previous result and the third element of the list, i.e. func(func(s1, s2),s3)
# - The list looks like this now: [ func(func(s1, s2),s3), ... , sn ]
# - Continue like this until just one element is left and return this element as the result of reduce()
from functools import reduce
print(list(map(lambda x,y : x+y, [47,11,42,13],[1])))
functools.reduce(lambda x,y : x+y, [47,11,42,13])
dir(functools)
f = lambda a,b: a if (a <= b) else b
s=[47,11,42,102,13]
# print(s.max)
reduce(f,s)
reduce(f, [11, 47,11,42,102,13])
reduce(lambda x, y: x+y, range(1,101))
reduce(lambda x, y: x*y, range(1,4))

reduce(lambda x, y: x*y, range(44,50))/reduce(lambda x, y: x*y, range(1,7))

acct= {'Order_Number': [34587, 98762, 77226, 88112],
       'Title_Author': ["Learning Python, Mark Lutz", 'Programming Python, Mark Lutz', "Head First Python, Paul Barry", "Einführung in Python3, Bernd Klein"],
       "Quantity": [4,5,3,3], "Price per Item": [40.95, 56.80, 32.95, 24.99]}

print(acct)
import pandas as pd
from functools import reduce
orders = pd.DataFrame(acct)
print(orders)

min_order=100

orders = [ ["34587", "Learning Python, Mark Lutz", 4, 40.95],
	       ["98762", "Programming Python, Mark Lutz", 5, 56.80],
           ["77226", "Head First Python, Paul Barry", 3,32.95],
           ["88112", "Einführung in Python3, Bernd Klein", 	3, 24.99]]

min_order = 100
invoice_totals = list(map(lambda x: x if x[1] >= min_order else (x[0], x[1] + 10), map(lambda x: (x[0],x[2] * x[3]), orders)))
total = list(map(lambda x: x if x[1] >= min_order else (x[0], x[1]+10), map(lambda x: (x[0], x[2] * x[3]), orders)))
print(invoice_totals)
print(total)

newinvo = list(map(lambda x: x if x[1] >= min_order else (x[0], x[1]+10), map(lambda x: (x[0], x[2] * x[3]), orders)))

orders = [ [1, ("5464", 4, 9.99), ("8274",18,12.99), ("9744", 9, 44.95)],
	       [2, ("5464", 9, 9.99), ("9744", 9, 44.95)],
	       [3, ("5464", 9, 9.99), ("88112", 11, 24.99)],
           [4, ("8732", 7, 11.99), ("7733",11,18.99), ("88112", 5, 39.95)] ]

ordernumber, (article number, quantity, price per unit)

print(orders[0][1][:])

tuples(order no., total amount)


min_order = 100
invoice_totals = list(map(lambda x: [x[0]] + list(map(lambda y: y[1]*y[2], x[1:])), orders))
print(invoice_totals)
invoice_totals = list(map(lambda x: [x[0]] + [reduce(lambda a,b: a + b, x[1:])], invoice_totals))
print(invoice_totals)
invoice_totals = list(map(lambda x: x if x[1] >= min_order else (x[0], x[1] + 10), invoice_totals))
print(invoice_totals)


print(pd.DataFrame(orders))


""" Define Column labels """
acct= {'Order_Number': [34587, 98762, 77226, 88112],
       'Title_Author': ["Learning Python, Mark Lutz", 'Programming Python, Mark Lutz', "Head First Python, Paul Barry", "Einführung in Python3, Bernd Klein"],
       "Quantity": [4,5,3,3], "Price per Item": [40.95, 56.80, 32.95, 24.99]}
