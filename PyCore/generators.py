cities = ["Paris", "Berlin", "Hamburg", "Frankfurt", "London", "Vienna", "Amsterdam", "Den Haag"]
for location in cities:
    print("location: " + location)

expertises = ["Novice", "Beginner", "Intermediate", "Proficient", "Experienced", "Advanced"]
expertises_iterator = iter(expertises)
next(expertises_iterator)

other_cities = ["Strasbourg", "Freiburg", "Stuttgart",
                "Vienna / Wien", "Hannover", "Berlin",
                "Zurich"]

city_iterator = iter(other_cities)
while city_iterator:
    try:
        city = next(city_iterator)
        print(city)
    except StopIteration:
        break

capitals = { "France":"Paris", "Netherlands":"Amsterdam", "Germany":"Berlin", "Switzerland":"Bern", "Austria":"Vienna"}
for country in capitals:
    print("The capital city of " + country + " is " + capitals[country])
print(city)

for x , y in zip(country ,capitals[country]):
    print(x,y)

def city_generator():
    yield("London")
    yield("Hamburg")
    yield("Konstanz")
    yield("Amsterdam")
    yield("Berlin")
    yield("Zurich")
    yield("Schaffhausen")
    yield("Stuttgart")

city=city_generator()
print(list(city))
print(next(city))

""" Method of Operation """
# Fn = Fn - 1 + Fn - 2
# with the seed values:
# F0 = 0 and F1 = 1

def fibonacci(n):
    """ A generator for creating the Fibonacci numbers """
    a, b, counter = 0, 1, 0
    while True:
        if (counter > n):
            return
        yield a
        a, b = b, a + b
        counter += 1
f = fibonacci(5)
for x in f:
    print(x, " ", end="") #
print()
def fibonacci():
    """Generates an infinite sequence of Fibonacci numbers on demand"""
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b

f = fibonacci()

counter = 0
for x in f:
    print(x, " ", end="")
    counter += 1
    if (counter > 10):
        break
print()

""" return generator """
def gen():
    yield 1
    raise StopIteration(42)

g = gen()

next(g)
next(g)

"""send Method /Coroutines """

def simple_coroutine():
     print("coroutine has been started!")
     x = yield
     print("coroutine received: ", x)

cr = simple_coroutine()
cr

next(cr)
coroutine has been started!
cr.send("Hi")
coroutine received:  Hi

def infinite_looper(objects):
    count = 0
    while True:
        if count >= len(objects):
            count = 0
        message = yield objects[count]
        if message != None:
            count = 0 if message < 0 else message
        else:
            count += 1

x = infinite_looper("A string with some words")
next(x)
x.send(9)

""" The throw Method """

def infinite_looper(objects):
    count = 0
    while True:
        if count >= len(objects):
            count = 0
        try:
            message = yield objects[count]
        except Exception:
            print("index: " + str(count))
        if message != None:
            count = 0 if message < 0 else message
        else:
            count += 1

looper = infinite_looper("Python")
next(looper)
looper.throw(Exception) # Returns the index of the current object and the next object

# We can improve the previous example by defining our own exception class StateOfGenerator:

class StateOfGenerator(Exception):
     def __init__(self, message=None):
         self.message = message

def infinite_looper(objects):
    count = 0
    while True:
        if count >= len(objects):
            count = 0
        try:
            message = yield objects[count]
        except StateOfGenerator:
            print("index: " + str(count))
        if message != None:
            count = 0 if message < 0 else message
        else:
            count += 1

looper = infinite_looper("Python")
next(looper)
looper.throw(StateOfGenerator)


""" Decorating Generators """
from functools import wraps

def get_ready(gen):
    """
    Decorator: gets a generator gen ready by advancing to first yield statement
    """
    @wraps(gen)
    def generator(*args,**kwargs):
        g = gen(*args,**kwargs)
        next(g)
        return g
    return generator

get_ready
def infinite_looper(objects):
    count = -1
    message = yield None
    while True:
        count += 1
        if message != None:
            count = 0 if message < 0 else message
        if count >= len(objects):
            count = 0
        message = yield objects[count]


x = infinite_looper("abcdef")
print(next(x))
print(x.send(4))
print(next(x))
print(next(x))
print(x.send(5))
print(next(x))

""" Yield from """
def gen1():
    for char in "Python":
        yield char
    for i in range(5):
        yield i

def gen2():
    yield from "Python"
    yield from range(5)

g1 = gen1()
g2 = gen2()
print("g1: ", end=", ")
for x in g1:
    print(x, end=", ")
print("\ng2: ", end=", ")
for x in g2:
    print(x, end=", ")
print()

"""################### ###################"""
def cities():
    for city in ["Berlin", "Hamburg", "Munich", "Freiburg"]:
        yield city

def squares():
    for number in range(10):
        yield number ** 2

def generator_all_in_one():
    for city in cities():
        yield city
    for number in squares():
        yield number

def generator_splitted():
    yield from cities()
    yield from squares()

lst1 = [el for el in generator_all_in_one()]
lst2 = [el for el in generator_splitted()]
print(lst1 == lst2)
print(lst1,"\n", lst2)


""" Recursive Generators """

def permutations(items):
    n = len(items)
    if n==0: yield []
    else:
        for i in range(len(items)):
            for cc in permutations(items[:i]+items[i+1:]):
                yield [items[i]]+cc

for p in permutations(['r','e','d']): print(''.join(p))
for p in permutations(list("game")): print(''.join(p) + ", ", end="")

import itertools
perms = itertools.permutations(['r','e','d'])
perms

list(perms)

def k_permutations(items, n):
    if n==0:
        yield []
    else:
        for item in items:
            for kp in k_permutations(items, n-1):
                if item not in kp:
                    yield [item] + kp

for kp in k_permutations("abcd", 4):
    print(kp)

#!/usr/bin/env python3
list(fibonacci())

def fibonacci():
    """Ein Fibonacci-Zahlen-Generator"""
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b

def firstn(g, n):
	for i in range(n):
		yield next(g)

print(list(firstn(fibonacci(), 10)))

a generator which computes the running average.

def running_average():
    total = 0.0
    counter = 0
    average = None
    while True:
        term = yield average
        total += term
        counter += 1
        average = total/counter

def running_average():
    total = 0.0
    counter = 0
    average = None
    while True:
        term = yield average
        total += term
        counter += 1
        average = total / counter


ra = running_average()  # initialize the coroutine
next(ra)                # we have to start the coroutine

for value in [7, 13, 17, 231, 12, 8, 3]:
    out_str = "sent: {val:3d}, new average: {avg:6.2f}"
    print(out_str.format(val=value, avg=ra.send(value)))

values=[7, 13, 17, 231, 12, 8, 3]
out_str = "sent: {val:3d}, new average: {avg:6.2f}"
print(out_str.format(val=value, avg=ra.send(values)))





for time in trange((10, 10, 10), (13, 50, 15), (0, 15, 12) ):
        print(time)

def trange(start, stop, step):
    """
    trange(stop) -> time as a 3-tuple (hours, minutes, seconds)
    trange(start, stop[, step]) -> time tuple

    start: time tuple (hours, minutes, seconds)
    stop: time tuple
    step: time tuple

    returns a sequence of time tuples from start to stop incremented by step
    """

    current = list(start)
    while current < list(stop):
        yield tuple(current)
        seconds = step[2] + current[2]
        min_borrow = 0
        hours_borrow = 0
        if seconds < 60:
            current[2] = seconds
        else:
            current[2] = seconds - 60
            min_borrow = 1
        minutes = step[1] + current[1] + min_borrow
        if minutes < 60:
            current[1] = minutes
        else:
            current[1] = minutes - 60
            hours_borrow = 1
        hours = step[0] + current[0] + hours_borrow
        if hours < 24:
            current[0] = hours
        else:
            current[0] = hours -24

if __name__ == "__main__":
    for time in trange((10, 10, 10), (13, 50, 15), (0, 15, 12) ):
        print(time)


squares = (x * x for x in range(1,10))
print(list(squares))
