class Car:
    def __init__(self, w):
        self.wheels = w

    def getWheel(self):
        return self.wheels

c1 = Car(4)
print("c1 memory location: ", hex(id(c1)))

import weakref
r = weakref.ref(c1)
print ("before :", r)

c1 = None
print("After :", r)

print("Garbage collected immediately!")


""" Memory Allocation Summary
- methods and variables are created on stack memoryview
- objects and instance variables are created on heap memory
- a new stack frame is created on onvocaiton of a function / method.
- stack frames are destroyed as soon as function/method returns
- Garbage collector is a mechanism to clean up the dead objects.
Python uses zero-referece Garbage Collection..
"""
