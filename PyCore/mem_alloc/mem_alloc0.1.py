x = 10
print(type(x))

y = x
print(id(x), id(y))
if (id(x) == id(y)):
    print("x and y refer to the SAME Object.")
else:
    print("x and y refer to Different Objects.")

x = x + 1

if (id(x) == id(y)):
    print("x and y refer to the SAME Object.")
else:
    print("x and y refer to Different Objects.")

z = 10
if (id(z) == id(y)):
    print("z and y refer to the SAME Object.")
else:
    print("z and y refer to Different Objects.")

y = 5
