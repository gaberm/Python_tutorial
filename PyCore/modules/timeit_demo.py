from random import random
import types

def list_without_comprehension():
	l = []
	for i in xrange(1000):
		l.append(int(random()*100 % 100))
	return l

def list_with_comprehension():
	# 1K random numbers between 0 to 100
	l = [int(random()*100 % 100) for _ in xrange(1000)]
	return l


# operations on list_without_comprehension
def sort_list_without_comprehension():
	list_without_comprehension().sort()

def reverse_sort_list_without_comprehension():
	list_without_comprehension().sort(reverse=True)

def sorted_list_without_comprehension():
	sorted(list_without_comprehension())


# operations on list_with_comprehension
def sort_list_with_comprehension():
	list_with_comprehension().sort()

def reverse_sort_list_with_comprehension():
	list_with_comprehension().sort(reverse=True)

def sorted_list_with_comprehension():
	sorted(list_with_comprehension())


def main():
	objs = globals()
	funcs = []
	f = open("timeit_demo.sh", "w+")

	for objname in objs:
		if objname != 'main' and type(objs[objname]) == types.FunctionType:
			funcs.append(objname)
	funcs.sort()
	for func in funcs:
		f.write('''echo "Timing: %(funcname)s"
python -m timeit "import timeit_demo; timeit_demo.%(funcname)s();"\n\n
echo "------------------------------------------------------------"
''' % dict(
				funcname = func,
				)
			)

	f.close()

if __name__ == "__main__":
	main()

	from os import system

	#Works only for *nix platforms
	system("/bin/bash timeit_demo.sh")

	#un-comment below for windows
	#system("cmd timeit_demo.sh")
