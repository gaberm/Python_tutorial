import pandas as pd


#Environment defintion!!
# pd.options.mode.chained_assignment = None  # default='warn'
pd.options.display.max_rows = 100
pd.options.display.max_colwidth=120
pd.options.display.width=160
pd.options.display.show_dimensions=True


def chunck_generator(filename, win_size):
    for chunk in pd.read_table(filename,delimiter=',', iterator=True, chunksize=win_size, usecols=['Message'], index_col=None ):
        yield (chunk)
    print("Print Chunk size: {}".format(win_size))

def generator( filename, chunk_size):
    chunk = chunck_generator(filename, chunk_size)
    for row in chunk:
        yield row

def iter(chunk_size):
    # print(chunk_size)
    # chunk_size = 15
    filename = r'C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\input\\input_small.csv'
    generate = generator(filename, chunk_size)
    try:
        while True:
           # print(next(generate))
           # print("------- Seperator -------")
           df = next(generate)
           print(df)
           # print("------- Seperator -------")
    except StopIteration:
        pass
    # do something at the end of the file
    return df


if __name__ == '__main__'d:
    iter(10)
else:
    pass
    print("Run First Module from an Import")

""" For a remote call..
# sys.path.append('./modules')
# import readchunk
from modules import readchunk
import sys


readchunk.iter(21)
"""
