# https://www.kaggle.com/yuliagm/how-to-work-with-big-datasets-on-16g-ram-dask
"""

    TIP 1 - Deleting unused variables and gc.collect()
    TIP 2 - Presetting the datatypes
    TIP 3 - Importing selected rows of the a file (including generating your own subsamples)
    TIP 4 - Importing in batches and processing each individually
    TIP 5 - Importing just selected columns
    TIP 6 - Creative data processing
    TIP 7 - Using Dask
"""
import numpy as np
import pandas as pd
import datetime
import os
import timeit
import matplotlib.pyplot as plt
import seaborn as sns
import gc
%matplotlib inline
# %timeit inline

#make wider graphs
sns.set(rc={'figure.figsize':(12,5)});
plt.figure(figsize=(12,5));

# eg:
#import some file
temp = pd.read_csv('../input/train_sample.csv')

#do something to the file
temp['os'] = temp['os'].astype('str')

#delete when no longer needed
del temp
#collect residual garbage
gc.collect()


"""
    DataFrame.apply : Perform any type of operations.
    DataFrame.transform : Perform transformation type operations.
    pandas.core.groupby.GroupBy : Perform operations over groups.
    pandas.core.resample.Resampler : Perform operations over resampled bins.
    pandas.core.window.Rolling : Perform operations over rolling window.
"""
