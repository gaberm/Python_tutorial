""" a similar problem here is a solution based on Pandas """

import pandas as pd

# ifname='.\input\input_small.csv'
# ofname = '.\input\output.csv'

ifname = 'C:\\Users\\Mohammed\\gitlab\\insights\input\input.csv'
ofname = 'C:\\Users\\Mohammed\\gitlab\\insights\input\output.csv'
# pwd ; cd C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\




def process_file(path,binSize):

    for chunk in pd.read_csv(path, sep=',', chunksize=binSize): #
        # print(chunk)
        print(chunk.iloc[:,2]) # get 3rd col
        # Do something with chunk....
        # chunk.dtypes
        # chunk.memory_usage()

if __name__ == '__main__':
    path=ifname
    binSize=5
    process_file(path,binSize)
