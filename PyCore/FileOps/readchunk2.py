import pandas as pd
import dask.dataframe as dd

chunksize = 10
fname='.\input\input.csv'

df = dd.read_csv(fname)
df=df.groupby(df.Message).Message.sum()

# print(df.head(20))

def get_from_action_data(fname, chunk_size):
    reader = pd.read_csv(fname, header=0, iterator=True, chunksize=chunk_size)
    chunks = []
    loop = True
    while loop:
        try:
            chunk = reader.get_chunk(chunk_size)[["Message"]]
            chunks.append(chunk)
        except StopIteration:
            loop = False
            print("Iteration is stopped")

    df_ac = pd.concat(chunks, ignore_index=True)
    return df_ac

df2 = get_from_action_data(fname, chunksize)
print(df2.head(100))
print(df2.shape)

# from  sklearn.linear_model import LinearRegression
#
# def train(partition):
#     est = LinearRegression()
#     est.fit(partition[['Message']], partition.Message.values)
#     return est
#
# train(df)
