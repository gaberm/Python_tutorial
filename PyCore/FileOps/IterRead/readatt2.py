import os
import pandas as pd
import timeit
import re
# import readloop_chunk as loopit
# import readiter_chunk as iterit
from unicodedata import normalize


### Environment setting
os.chdir('C:\\Users\\Mohammed\\github\\fuzzy')

### Script Variables setting
chunk = 120
file = 'C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\input\\input_small.csv'
fname = open(file, mode='r')
regex = re.compile(r"[0-9]{3,}", re.IGNORECASE)


def normFn(text):
    #msgdata.Time=pd.to_datetime(msgdata.Time).dt.strftime('%Y%m%d_%H:%M')
    # PIN Digits Cleanup
    # msgdata.Message=msgdata.Message.replace("[0-9]{3,}", 'XX')
    # msgdata.Message=re.sub(r"[0-9]{4,}", "XX", msgdata.Message)
    # msgdata.str.replace("[0-9]{4,}",RegEX=True)
    # nrstr = normalize('NFD', input)
    text = normalize('NFC', text)
    # nrstr=nrstr.replace(regex, 'XX')normstring = normstring.lower()
    text=re.sub(regex, 'XX', text)

    #Sort DataFrame
    #msgdata.sort_values(by['source', 'message','time'], inplace=True)
    #msgdata.groupby(msgdata.Message)
    # s1 = normalize('NFD', u1)
    return text

def readFile():
    return fname.read(chunk)

def process_data(input):
    # str=input
    # print(input)
    # print("#### Seperator ###")

    normstr=normFn(input)
    print(normstr)

    # print("### Seperator ###")
    # print(type(normstr))
    # msgdata = pd.read_csv(fname, usecols=['Time','Source','Message'],
    #                   parse_dates=['Time'], index_col=None,
    #                   dtype={"Source": str, "Message": str}, infer_datetime_format=True )

    # df = pandas.read_table(nrstr, li, names=('A', 'B', 'C'))
    # df = pd.read_table(nrstr, delimiter=",",  sep=',', usecols=["Time", "Source", "Message"],
                    # iterator=True, engine= 'c', names =["Time", "Source", "Message"], parse_dates = True)#, chunksize=1000,)


for piece in iter(readFile, ''):
    process_data(piece)
