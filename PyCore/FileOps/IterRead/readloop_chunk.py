import os
import timeit
## Option 1

# os.chdir(os.path.dirname(sys.argv[0]))
# os.chdir('C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\input')
# %pwd
# fname='.\input\input_small.csv'
t1= timeit.default_timer()

def read_in_chunks(file_object, chunk_size=100):
    """Lazy function (generator) to read a file piece by piece.
    Default chunk size: 1k."""
    while True:
        data = file_object.read(chunk_size)
        if not data:
            break
        yield data

# data = read_in_chunks(fname)
# print(data)
# fname=open('C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\input\\input_small.csv')
# fname = open('C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\input\\input.csv')
# fname = open('C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\input\\input60k.csv')
fname = open('C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\input\\input_original.csv')
for piece in read_in_chunks(fname):
    str=input
    # print(piece)
    # print("Seperator \#\#\#\#")
    # next(piece)
#
# f = open('really_big_file.dat')
# def read1k():
#     return f.read(1024)
#
# for piece in iter(read1k, ''):
#     process_data(piece)
#
t2= timeit.default_timer()
print("Time of execution is: ", (t2-t1)*1000, " msecs")
