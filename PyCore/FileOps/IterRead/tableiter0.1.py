import pandas as pd


chunk_size = 10
def chunck_generator(filename, header=False,chunk_size = 100):
   for chunk in pd.read_csv(filename,delimiter=',', iterator=True, chunksize=chunk_size, parse_dates=[1] ):
        yield (chunk)

def generator( filename, header=False,chunk_size = 5):
    chunk = chunck_generator(filename, header=False,chunk_size =  5)
    for row in chunk:
        yield row

if __name__ == "__main__":
# filename = r'file.csv'
    filename = '/mnt/store/home/GitLibrary/Insights/input/input_small.csv'
    generator = generator(filename=filename)
    while True:
       print(next(generator))
       print("------- Seperator -------")
    # else: print("end..")
    # StopIteration
    # except GeneratorExit:
        # print("Only made it to %d" % n)
