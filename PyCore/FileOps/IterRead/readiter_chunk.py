import os
import pandas as pd
import timeit
# os.chdir(os.path.dirname(sys.argv[0]))
# os.chdir('C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\input')
# %pwd
# fname='.\input\input_small.csv'
t1= timeit.default_timer()

# fname = open('C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\input\\input_small.csv')
# fname = open('C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\input\\input.csv')
# fname = open('C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\input\\input60k.csv')
fname = open('C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\input\\input_original.csv')
chunk = 160

def readFile():
    return fname.read(chunk)

def process_data(input):
    str=input
    # print(input)
    # print("Seperator \#\#\#\#")

for piece in iter(readFile, ''):
    process_data(piece)

# def __main__():
# def main():
#   print("Hello World!")

t2= timeit.default_timer()
print("Time of execution for Read Iteration is: ", (t2-t1)*1000, " msecs")
