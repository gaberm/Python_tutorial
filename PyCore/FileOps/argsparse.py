# https://docs.python.org/2/library/argparse.html
import argparse

parser = argparse.ArgumentParser(
    description='A program to provide statistics for rule list.', add_help=True)
# parser.add_argument('filename', type=str, nargs='+', help='source filename')
parser.add_argument('filename', type=str, help='source filename')
parser.add_argument(
    '-m', '--meta', help='choose metasearch (default method)', action='store_true')
parser.add_argument(
    '-f', '--fuzzy', help='choose fuzzysearch', action='store_true')

# verbosity implmentation with choices
# parser.add_argument("-v", "--verbosity", type=int,choices=[0, 1, 2], help="increase output verbosity")
# if args.verbosity == 2:
#     print "the square of {} equals {}".format(args.square, answer)
# elif args.verbosity == 1:
#     print "{}^2 == {}".format(args.square, answer)
# else:
#     print answer


def func1():
    print('This is meta func called with an arg or the lack of with file:', args.filename)


def func2():
    print('This is fuzzy func called with an arg with file:', args.filename)


# parser.print_help()
args = parser.parse_args()
print(args)
# print(args.filename[0])
# print(args.filename)
# print(args.meta)
if args.fuzzy:
    func2()
else:
    func1()


"""
argparse.ArgumentParser(
    ['prog=None', 'usage=None', 'description=None', 'epilog=None', 'parents=[]', "formatter_class=<class 'argparse.HelpFormatter'>", "prefix_chars='-'", 'fromfile_prefix_chars=None', 'argument_default=None', "conflict_handler='error'", 'add_help=True', 'allow_abbrev=True'],
)
Docstring:
Object for parsing command line strings into Python objects.

Keyword Arguments:
    - prog -- The name of the program (default: sys.argv[0])
    - usage -- A usage message (default: auto-generated from arguments)
    - description -- A description of what the program does
    - epilog -- Text following the argument descriptions
    - parents -- Parsers whose arguments should be copied into this one
    - formatter_class -- HelpFormatter class for printing help messages
    - prefix_chars -- Characters that prefix optional arguments
    - fromfile_prefix_chars -- Characters that prefix files containing
        additional arguments
    - argument_default -- The default value for all arguments
    - conflict_handler -- String indicating how to handle conflicts
    - add_help -- Add a -h/-help option
    - allow_abbrev -- Allow long options to be abbreviated unambiguously
"""
