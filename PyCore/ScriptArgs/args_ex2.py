# Write a function which calculates the arithmetic mean of a variable number of values
import sys

# Iteration over all arguments:
# for eachArg in sys.argv:
#         print(eachArg)

def arithmetic_mean(x, *l):
    """The function calculates the mean of a non-empty arbitary number of values"""
    # x = '5'
    # x.
    sum = int(x.split()[0])
    for args in l:
        sum += int(args)
    mean = sum / (1 + sys.argv.count)
    print( f'The sum is {sum} and the mean is {mean}')

arithmetic_mean(sys.argv[0], sys.argv[1:])
# print(sys.argv[0], sys.argv[1:])
# print(type(sys.argv), type(sys.argv[0]))
