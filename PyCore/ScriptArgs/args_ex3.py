""" Command line interface to overview.py providing a quick scan of the data provided based on the rules configured:

# * ndiff:    lists every line and highlights interline changes.
# * context:  highlights clusters of changes in a before/after format.
# * unified:  highlights clusters of changes in an inline format.
# * html:     generates side by side comparison with change highlights.

"""

import sys, os, time, difflib, optparse

def main():
     # Configure the option parser
    usage = "Command line interface to overview.py providing a quick scan of the data provided based on the rules configured\nusage: %prog [options] rulesfile datafile"
    parser = optparse.OptionParser(usage)
    parser.add_option("-r","--recent", action="store_true", default=False,
                      help='apply only the most recent rules (default)')
    parser.add_option("-m","--monitoring", action="store_true", default=False,
                      help='apply rules to current monitored traffic only (default)')
    parser.add_option("-a", "--all",  action="store_true", default=False, help='apply rules to all traffic')
    parser.add_option("-o", "--one-rule", action="store_true", default=False,
                      help='run with the specified rule only')

    (options, args) = parser.parse_args()

    if len(args) == 0:
        parser.print_help()
        sys.exit(1)
    if len(args) != 2:
        parser.error("need to specify both a rulesfile and datafile")

    fromfile, tofile = args # as specified in the usage string

    # we're passing these as arguments to the diff function
    # fromdate = time.ctime(os.stat(fromfile).st_mtime)
    # todate = time.ctime(os.stat(tofile).st_mtime)
    # with open(fromfile, 'U') as f:
    #     fromlines = f.readlines()
    # with open(tofile, 'U') as f:
    #     tolines = f.readlines()

    if options.monitoring:
        print('applying to monitor only traffic')
    elif options.all:
        print('applying to all traffic')
    elif options.recent:
        print('applying most recent rules only')
    elif options.one_rule:
        print('applying one rule only')
    else:
        pass

    # we're using writelines because diff is a generator
    # sys.stdout.writelines(diff)

if __name__ == '__main__':
    main()
