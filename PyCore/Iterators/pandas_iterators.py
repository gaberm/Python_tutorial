# https://stackoverflow.com/questions/43222878/iterate-over-pandas-dataframe-and-update-the-value-attributeerror-cant-set-a

"""Trying to iterate over a pandas dataframe and update the value if condition is met but i am getting an error.

for line, row in enumerate(df.itertuples(), 1):
    if row.Qty:
        if row.Qty == 1 and row.Price == 10:
            row.Buy = 1
AttributeError: can't set attribute """

import numpy as np
import pandas as pd


df = pd.DataFrame(data = {"a": np.random.randn(100), "b": np.random.random(100)*100} )
"""You should **never modify** something you are iterating over.
   This is not guaranteed to work in all cases. Depending on the data types, the iterator returns a copy and not a view, and writing to it will have no effect. """
next(df.iterrows())

for idx, row in df.iterrows():
    if  df.loc[idx,'b'] == 56.10329204899897:
        # print(idx)
        df.loc[idx,'a'] = 'Cought'

if 21.234956 in df['b']:
    print('found')

for idx, row in df.iterrows():
    print(idx, row)

df.loc[0, 'b']


df = pd.DataFrame(data = {"a": np.random.randn(100), "b": np.random.random(100)*100} )
df.loc[[0,1,72,73,74], 'a'] = 'xxx'
