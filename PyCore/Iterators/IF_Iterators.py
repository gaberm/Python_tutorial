"""Non Pyhtonic"""
a = [2,3,4,5,6,7,8,9,0]
xyz = [0,12,4,6,242,7,9]
for x in xyz:
    if x in a:
        print(x)
#0,4,6,7,9

print([x for x in xyz if x in a])
print(x for x in xyz if x in a)

gen = (x for x in xyz if x not in a)
for x in gen:
    print(x)

gen = (y for (x,y) in enumerate(xyz) if x not in a)
for i in gen:
    print(i)

for x in (x for x in xyz if x in a):
    print(x)

# The Pythonic way of getting the sorted intersection of two sets is:
sorted(set(a).intersection(xyz))
# [0, 4, 6, 7, 9]

# Or those elements that are xyz but not in a:
sorted(set(xyz).difference(a))
# [12, 242]

a = [2,3,4,5,6,7,8,9,0]
xyz = [0,12,4,6,242,7,9]
for x in filter(lambda w: w in a, xyz):
  print x

def wuzzy1(text):
    for i in range(len(text)):
         """A 2nd If condition to drop text if X exceeds 20% """
         if text.Message.iloc[i] != "yy" and (text.Message.iloc[i].count('X') / len(text.Message.iloc[i])) < 0.2:
         # if text.Message.iloc[i] != "yy" and (text.Message.iloc[i].count('X') / len(text.Message.iloc[i])) < 0.2:
         ## #  if any implementation would fit here nicely to replace iteration..
             xdf=pd.DataFrame(data=(process.extractBests(text.Message.iloc[i], text.Message, limit=None, score_cutoff=90)),
                          columns=["Message", "Accuracy", "Indexes"])
             # print(xdf.head(20))
             count_matches=len(xdf)
             #print(" {:120.120s}     ---     matches: {}".format( msgdata.Message[msgx], count_matches))
             results.append((text.Message.iloc[i], count_matches))
             # for x in range(count_matches): text.Message[xdf.Indexes[x]]="yy"
             for idx in xdf.iterrows():
                     df.loc[idx,'message'] = 'yy'



a = [2,3,4,5,6,7,8,9,0]
xyz = [0,12,4,6,242,7,9]

print([x for x in xyz if x in a])
for x in range(10):
    print(x)

[text.Message[xdf.Indexes[x]]='yy' for x in range(10)]


mask = (df['Qty'] == 1) & (df['Price'] == 10)
df.loc[mask, 'Buy'] = 1



['yy' for i in a if (i == 3 or i == 5)]
