### https://doughellmann.com/blog/2009/06/19/python-exception-handling-techniques/
#!/usr/bin/env python

def throws():
    raise RuntimeError('this is the error message')

def main():
    throws()

if __name__ == '__main__':
    main()
