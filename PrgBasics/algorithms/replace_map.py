# Function to replace a character c1 with c2
# and c2 with c1 in a string S

def replaceChars(input,c1,c2):

	# create lambda to replace c1 with c2, c2
	# with c1 and other will remain same
	# expression will be like "lambda x:
	# x if (x!=c1 and x!=c2) else c1 if (x==c2) else c2"
	# and map it onto each character of string
	newChars = map(lambda x: x if (x!=c1 and x!=c2) else c1 if (x==c2) else c2,input)

	# now join each character without space
	# to print resultant string
	print (''.join(newChars))

# Driver program
if __name__ == "__main__":
	input = 'grrksfoegrrks'
	c1 = 'e'
	c2 = 'r'
	replaceChars(input,c1,c2)


o = [0,Ö, ó, ò, ô, ō, õ]

normalisationmap=
0oO
1iIlL!\/&
2zZ
3eE
4aA
5sS
7tT
8bB
9gG
i, I, l, L, !, \, /
4, a, A, ä, Ä
5, s, S, $, ß
7, t, T
u, U, ü, Ü

string.replaceChars()
string.replace((x, "o") for x in ["O","0"])

string.replace(x, 'o' for x in ['0'])

squares = (x for x in ['O', '0'])
# squares = (x for x in (1,2,3))
type(squares)
print(list(squares))

string = "He110 W0r1d!"
string.replace((x, 'o' for x in ['O', '0']))
string.replace((x for x in ['O', '0']), 'o')


def double(L):
    return [x*2 for x in L]

eggs = double([1, 2, 3, 4, 5])
print(eggs)

[ ('XXF006E006A006FXXFXXEXXD000AXXB002EXXF006DXXFXXDXXCXXEXXDXXDXXE006FXX', 1), ('XXF006E006EXXA006FXXE00EXX', 1), ('XXEXXD000AXXFXXEXXD00EXXFXXF006D006DXXEXXAXXD000AXXEXXCXXFXXF006D006DXX',  1),  ('XXFXXEXXF006BXXDXXD000A004DXX', 1), ('XXCXXEXXEXXCXXFXXF006EXXEXXFXXFXXF006EXXDXXFXXF006D006D', 1),  ('006EXXAXXA002F002FXXB002EXXF006D002FXXF006FXXFXX', 1),  ('XXFXXEXXAXXAXXEXXF006EXXEXXAXX', 1)]

myString.Count(char.IsLetter);

myString = 'XXF91CC5DF45DFXXA86BXXED4FEXXB5F55FF0C9A8C8BCXXFF0C8BFXXFXXDXXA63D04EA49A8C8BCXXFF0CXXFF5C069A8C8BCXXCCXXE8E4ED64EBAXX'
myString.count('X')
len(myString)

myString.count('X') / len(myString)

def xfinder(myString, char):
	if (myString.count('X') / len(myString)) < 0.3:
		return myString

xfinder(myString, "X")
len(myString)/myString.count('X')
