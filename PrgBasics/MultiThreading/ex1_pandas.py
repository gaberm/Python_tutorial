import pandas as pd
import numpy as np
# import seaborn as sns
from multiprocessing import Pool
from multiprocessing import freeze_support
import os



def parallelize_dataframe(func, df):
    results = []
    df_split = np.array_split(df, num_partitions)
    pool = Pool(num_cores)
    df = pd.concat(pool.map(func, df_split))
    # results.append(pool.map(func, df_split))
    pool.close()
    pool.join()
    return results



def FuzzyMatch(subject):
    # init results
    results = []

    # split method to lower the load
    def recurse(array):
        if len(array) < 10:
            # print(f'Calling Fuzzy algorithm on current array {array}, and returning results..')
            x  = dumatch(array)
            results.append(x)

        else:
            split_array = np.split(array, [int(len(array)/2)], axis=0)
            # print(f'recursing to a lower level with {(len(split_array[0]))} levels'), recurse(split_array[0])
            # print(f'recursing to a lower level with {(len(split_array[0]))} levels'), recurse(split_array[1])
            recurse(split_array[0]); recurse(split_array[1])


    def dumatch(array):
        return [('G-XX is your Google verification code.', 23), ('XX is your BIGO LIVE verification code.', 15), ('Your YouTube Go verification code: XX', 12), ('WhatsApp code 650-565...You can also tap on this link to verify your phone: v.whatsapp.com/XX',  5)]

    recurse(subject)
    flatten = lambda l: [item for sublist in l for item in sublist]
    results_df = pd.DataFrame(list(flatten(results)), columns=['message', 'count'])
    results_df = results_df.groupby('message')['count'].sum()
    return results_df


# Tester values
# df = pd.DataFrame({"a": np.random.random(100), "b": np.random.random(100), "id": np.arange(100)})
# results = dumatch(df)



if __name__ == '__main__':
    num_partitions = 10 #number of partitions to split dataframe
    num_cores = 4 #number of cores on your machine

    # Change Path
    os.chdir("C:\\Users\\Mohammed\\GitLib\\Insights\\")
    ##load the CSV file
    data = pd.read_csv('.\input\input_small.csv', usecols=['Time','Source','Message'],
                      parse_dates=['Time'], index_col=None,
                      dtype={"Source": str, "Message": str}, infer_datetime_format=True )
    # data.describe

    # parallelize_dataframe(data, dumatch)
    results = FuzzyMatch(data)
    # parallelize_dataframe(data, FuzzyMatch)

    print(tabulate(results, tablefmt='psql'))
