import threading
import timeit

start=timeit.default_timer

def countdown(n):
    while n>0 :
        n -=1

def countdown2(count):
    t1 = threading.Thread(target=countdown, args=(count//2,))
    t2 = threading.Thread(target=countdown, args=(count//2,))
    t1.start()
    t2.start()
    t1.join()
    t2.join()

#count = 100000000  #100 million
# %timeit countdown(count)
# 6.61 s ± 981 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)
print("Sequential run: {}".format(timeit.timeit(countdown(100000000)))
print("Threaded run: {}".format( timeit.Timer(countdown2(100000000)) )
