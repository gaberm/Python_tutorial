# https://rob-bell.net/2009/06/a-beginners-guide-to-big-o-notation/
"""Big O notation is used in Computer Science to describe the performance or complexity of an algorithm. Big O specifically describes the worst-case scenario, and can be used to describe the execution time required or the space used (e.g. in memory or on disk) by an algorithm.

Anyone who's read Programming Pearls or any other Computer Science books and doesn’t have a grounding in Mathematics will have hit a wall when they reached chapters that mention O(N log N) or other seemingly crazy syntax. Hopefully this article will help you gain an understanding of the basics of Big O and Logarithms.

As a programmer first and a mathematician second (or maybe third or fourth) I found the best way to understand Big O thoroughly was to produce some examples in code. So, below are some common orders of growth along with descriptions and examples where possible."""

# O(1)
# O(1) describes an algorithm that will always execute in the same time (or space) regardless of the size of the input data set.

def IsFirstElementNull(elements):
    return elements[0] == None;

l = [None, 0, 1, 2]
%timeit IsFirstElementNull(l)

m = l* 50
%timeit IsFirstElementNull(m)

n = m * 5000
%timeit IsFirstElementNull(n)


// # O(N)
// # O(N) describes an algorithm whose performance will grow linearly and in direct proportion to the size of the input data set. The example below also demonstrates how Big O favours the worst-case performance scenario; a matching string could be found during any iteration of the for loop and the function would return early, but Big O notation will always assume the upper limit where the algorithm will perform the maximum number of iterations.

def ContainsValue(elements, value):
    for element in elements:
        if (element == value): return True
    return False;

%timeit ContainsValue(l, 4)
%timeit ContainsValue(m, 4)
%timeit ContainsValue(n, 4)

# // O(N2)
# // O(N2) represents an algorithm whose performance is directly proportional to the square of the size of the input data set. This is common with algorithms that involve nested iterations over the data set. Deeper nested iterations will result in O(N3), O(N4) etc.

def ContainsDups(elements, value):
    outer = 0
    while outer < (len(elements) - 1):
        outer +=1
        inner = 0
        while inner < (len(elements) - 1):
            inner +=1
            # // Don't compare with self
            if (outer == inner): continue
        # print(inner, outer)
        if (elements[outer] == elements[inner]):
            return True
    return False

l = [0,1,2,3,4]
m = l * 100
n = m * 5000

%timeit ContainsDups(l, 3)
%timeit ContainsDups(m, 3)
%timeit ContainsDups(n, 3)


# O(2N)
# O(2N) denotes an algorithm whose growth doubles with each additon to the input data set. The growth curve of an O(2N) function is exponential - starting off very shallow, then rising meteorically. An example of an O(2N) function is the recursive calculation of Fibonacci numbers:

def Fibonacci(n):
    if n == 0:
        return 1
    else:
        return n * Fibonacci(n-1)

def iterative_Fibonacci(n):
    result = 1
    for i in range(2,n+1):
        result *= i
    return result

%timeit Fibonacci(5)
%timeit iterative_Fibonacci(5)
%timeit Fibonacci(500)
%timeit iterative_Fibonacci(500)
%timeit Fibonacci(999)
%timeit iterative_Fibonacci(999)
