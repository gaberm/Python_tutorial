""" Trying different Frequency Count methods for performance improvement.
So far I have a problem losing some texts ( ex. facebook)
Trying to know if there is a limitaiton on string variable that causes this behavior!!"""

import nltk
import re
import unicodedata
import pandas as pd
import string
# import urllib.request
# from bs4 import BeautifulSoup
from nltk.corpus import stopwords
import pdb
pdb.pm

# fname='.\input\input_small.csv'
fname='C:\\Users\\Mohammed\\gitlab\\insights\\input\input.csv'


def LoadFile():
    dataset = pd.read_csv(fname, engine= 'c', sep=',', usecols=['Message'] )#, chunksize=1000 )
    return dataset
    # text = str (dataset)
    # return text

def textSplit(d):
    result = []
    word = ''

    for char in d:
        if char not in string.whitespace:
            if char not in string.ascii_letters + "'":
                if word:
                        result.append(word)
                result.append(char)
                word = ''
            else:
                word = ''.join([word,char])

        else:
            if word:
                result.append(word)
                word = ''
    return result

def tokenizer(text):
    tokens = nltk.word_tokenize(text)
    return tokens


def normFn(inputdf):
    """clean special chars and extra whitespace"""
    # import pdb;pdb.set_trace()
    # inputdf.Message = re.sub("\W", "", inputdf.Message).strip()
    # sub(​pattern, repl, string​)
    # print re.sub(r"[ab]", "z", "abcABC", flags=re.IGNORECASE)
    inputdf.Message = inputdf.Message.str.lower()
    # inputdf.Message = inputdf.Message.str.strip()
    inputdf.Message = inputdf.Message.str.replace("[0-9]{2,}", 'XX')
    # inputdf.Message = inputdf.Message.str.replace("\\.", " " )#.split("\\s+")
    # Striping (non-relevant) punctuation
    # inputdf.Message.translate(str.maketrans("", "", ",.-'\"():;+/?$°@"))
    # inputdf.Message = inputdf.Message.str.replace(r"[\s\n{}]{2,}|[^\w\s]", ' ')

    # inputdf["Facebook"] = inputdf.astype(str).sum(axis=1).str.contains(r"[Ff]acebook")
    # print( inputdf.loc[inputdf['Facebook'] == True] )

    # lambda x: 'value if condition is met' if x condition else 'value if condition is not met'
    # found=inputdf.Message.str.find("google")
    # print(type(found))
    # l <- sapply(colnames(df), function(x) grep("horse", df[,x]))

    # Stripping accents
    # inputdf = ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
    # import pdb; pdb.set_trace()
    return inputdf


def tknFn(text):
    ### Tokenization and removing Stop Words
    search = ['google','facebook','whatsapp','viber', 'twitter','snapchat', 'telegram', 'instagram' ]
    tokens = tokenizer(text)

    for i in search:
        if i in text: print(i)
    # tokens = [item for item in map(str.strip, re.split("(\W+)", text)) if len(item) > 0]
    # tokens4 = [t for t in text.split()]
    # tokens2 = re.findall(r"[\w']+|[.,!?;]", text)
    # tokens3 = re.findall(r"\w+|[^\w\s]", text, re.UNICODE)
    # print(tokens)
    # print(type(tokens))
    # matching = [s for s in tokens if 'twitter' in s]
    # print(matching)
    # breakpoint()
    # clean_tokens = tokens[:]
    # sr = stopwords.words('english')
    #
    # for token in tokens:
    #     if token in stopwords.words('english'):
    #         clean_tokens.remove(token)
    # freq = nltk.FreqDist(clean_tokens, )
    # found=text.find("google")
    # print(found)
    freq = nltk.FreqDist(tokens)
    # for key,val in freq.items():
    #     if val > 10 :
    #         print (str(key) + ':' + str(val))
    print("--------------- Sep ---------------")
    for key in search:
        print (key ,freq.get(key))


    return freq


rinput = LoadFile()
inputdf = normFn(rinput)
text = str(inputdf['Message'])
freq = tknFn(text)
# print(list(freq))
# freq.plot(20, cumulative=False)
