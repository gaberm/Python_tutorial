""" Tokenization benchmarking using MultiProcessing"""

import spacy
from nltk.tokenize.moses import MosesTokenizer
import time
import multiprocessing as mp
from mxnet.gluon.data.text import IMDB

spacy_handler = spacy.load('en')

moses_tokenizer = MosesTokenizer()


def spacy_tokenize1(sample):
    nlp = spacy.load('en')
    return [ele.text for ele in nlp(sample)]


def spacy_tokenize2(sample):
    return [ele.text for ele in spacy_handler(sample)]


def spacy_tokenize3(sample):
    return [ele.text for ele in spacy_handler(sample, disable=['parser', 'tagger', 'ner'])]


moses_tokenize1 = MosesTokenizer().tokenize
moses_tokenize2 = moses_tokenizer.tokenize

def test_multiprocess_tokenization(num_worker=4):
    data_train = IMDB('train')
    data_train = data_train.transform(lambda sample: sample[0])
    total_word_num = sum(data_train.transform(lambda ele: len(str.split(ele))))
    if num_worker is None:
        start = time.time()
        sentences = list(map(moses_tokenize2, data_train))
        end = time.time()
    else:
        start = time.time()
        with mp.Pool(num_worker) as pool:
            sentences = list(pool.map(moses_tokenize2, data_train))
        end = time.time()
    wos = total_word_num / 1000 / (end - start)
    if num_worker is not None:
        wos_per_worker = "%g" % (wos / num_worker)
    else:
        wos_per_worker = "None"
    print("Time spent for tokenizing IMDB using %s workers: %g, wos=%g K, wos per worker=%s" %
          (str(num_worker), end - start, wos, wos_per_worker))
    return sentences


def sentences_same(sentences1, sentences2):
    for sent1, sent2 in zip(sentences1, sentences2):
        if ''.join(sent1) != ''.join(sent2):
            return False
    return True

baseline_sentences = test_multiprocess_tokenization(None)
for num_worker in range(1, 9):
    sentences = test_multiprocess_tokenization(num_worker)
    assert(sentences_same(baseline_sentences, sentences))
