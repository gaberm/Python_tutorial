""" Tokenization benchmarking using MultiProcessing"""

import spacy
from nltk.tokenize.moses import MosesTokenizer
import time
from mxnet.gluon.data.text import IMDB

spacy_handler = spacy.load('en')

moses_tokenizer = MosesTokenizer()


def spacy_tokenize1(sample):
    nlp = spacy.load('en')
    return [ele.text for ele in nlp(sample)]

def spacy_tokenize2(sample):
    return [ele.text for ele in spacy_handler(sample)]


def spacy_tokenize3(sample):
    return [ele.text for ele in spacy_handler(sample, disable=['parser', 'tagger', 'ner'])]


moses_tokenize1 = MosesTokenizer().tokenize
moses_tokenize2 = moses_tokenizer.tokenize

data_train = IMDB('train')
data_train = data_train.transform(lambda sample: tuple(sample), lazy=False)
N = 10
for method_name, func in [('spacy1', spacy_tokenize1), ('spacy2', spacy_tokenize2), ('spacy3', spacy_tokenize3),
                          ('moses1', moses_tokenize1), ('moses2', moses_tokenize2),
                          ('str.split', str.split)]:
    total_word_num = 0
    for i in range(N):
        total_word_num += len(data_train[i][0].split(' '))
    start = time.time()
    for i in range(N):
        new_dat = func(data_train[i][0])
    end = time.time()
    print("Method: %s, Tokenized %d sentences, Total Time Spent: %g, word per second (wps) = %g K"
          % (method_name, N, end - start, total_word_num / 1000 / (end - start)))
