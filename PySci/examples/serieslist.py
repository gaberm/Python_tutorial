import pandas as pd

## Create list
list_var = ['a', 'b', 'c', 'd']
print(list_var)

## Create a Series
series_var = pd.Series(list_var, index = [2,1,3,0])
print(series_var)

print(series_var[0])
print(list_var[0])

## Series like a Dictionary
series_var[2] = 'f'
print(series_var)

## Series like a vector
list_var = [1, 2 ,3 ,4]
series_var = pd.Series(list_var)

print(list_var*2)
print(series_var*2)

## Converting a list to a series
student_marks = {'name': ['sanad', 'shubam', 'faizan'], 'marks': [98, 100, 74]}
df = pd.DataFrame(student_marks)

balloons_data = [ {'red':1 , 'blue': '2'},
                 {'blue': 4, 'green': '1'},
                 {'red':2, 'blue' : '4'}]
print(balloons_data)
df = pd.DataFrame(balloons_data)
print(df)
