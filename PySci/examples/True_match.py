#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Libraries import!!
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import re

df=pd.read_csv('.\input\input.csv', parse_dates=True)

Search_for_These_values = ['PEA', 'DEF', 'XYZ'] #creating list

pattern = '|'.join(Search_for_These_values)     # joining list for comparision

IScritical=df['Message'].str.contains(pattern)
#msgdata.Message=msgdata.Message.str.replace("[0-9]{4,}", 'XX')

df['NEWcolumn'] = df['COLUMN_to_Check'].str.contains(pattern)
df['NEWcolumn'] = df['NEWcolumn'].map({True: 'Yes', False: 'No'})


# Source
# https://datascience.stackexchange.com/questions/28608/using-pandas-check-a-column-for-matching-text-and-update-new-column-if-true
