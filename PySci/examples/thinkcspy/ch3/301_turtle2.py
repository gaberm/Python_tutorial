import turtle
wn = turtle.Screen() # Set up the window and its attributes

wn.bgcolor("lightgreen")
wn.title("Tess & Alex")

tess = turtle.Turtle() # Create tess and set some attributes
tess.color("hotpink")
tess.pensize(5)

alex = turtle.Turtle() # Create alex
alex.shape("turtle")
alex.speed(10)
alex.pensize(3)

colors = ["yellow", "red", "purple", "blue"]
for c in colors:
    alex.color(c)
    alex.forward(150)
    alex.left(90)

alex.color("black")
wn.exitonclick()
