class Point:
    """ Point class represents and manipulates x,y coords. """
    def __init__(self, x = 0, y = 0):
        """ Create a new point at the origin x,y """
        self.y = y
        self.x = x

    def distance_from_origin(self):
        """ Compute my distance from the origin (0,0) """
        return ((self.x ** 2) + (self.y ** 2)) ** 0.5

    def __str__(self):
        """ Print the point in a tuble format """
        print("({0}, {1})".format(pt.x, pt.y))

    def halfway(self, target):
        """ Return the halfway point between myself and the target """
        mx = (self.x + target.x)/2
        my = (self.y + target.y)/2
        return Point(mx, my)





p = Point() # Instantiate an object of type Point
p.x=3
p.y=4
q = Point() # Make a second point


print( "P(",p.x,",", p.y,") and Q(", q.x,",",q.y, ")" ) # Each point object has its own x and y
input("Press Enter to continue!")
