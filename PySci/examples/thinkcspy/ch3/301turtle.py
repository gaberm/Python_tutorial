"""
#V1
import turtle # Allows us to use turtles
wn = turtle.Screen() # Creates a playground for turtles
alex = turtle.Turtle() # Create a turtle, assign to alex
alex.forward(50) # Tell alex to move forward by 50 units
alex.right(90) # Tell alex to turn by 90 degrees
alex.forward(50) # Complete the second side of a rectangle
alex.color("red")
alex.right(180)
ruby= turtle.Turtle()
ruby.color("blue")
ruby.backward(50)
ruby.right(90)
ruby.backward(50)
#
# Hack: wn.mainloop() # Wait for user to close window

#V2
BKColor= input("Please enter a color for the background! :")
wn.bgcolor(BKColor)# Set the window background color
wn.title("Hello, Tess!") # Set the window title

tess = turtle.Turtle()
TSColor= input("Please enter Tess Color! :")
tess.color(TSColor) # Tell tess to change her color
TSPensize=input("Please enter Tess Size! :")
tess.pensize(width=int(TSPensize)) # Tell tess to set her pen width
tess.forward(50)
tess.left(120)
tess.forward(50)
"""

import turtle
wn = turtle.Screen() # Set up the window and its attributes
wn.bgcolor("lightgreen")
wn.title("Tess & Alex")
tess = turtle.Turtle() # Create tess and set some attributes
tess.color("hotpink")
tess.pensize(5)

alex = turtle.Turtle() # Create alex
tess.forward(80) # Make tess draw equilateral triangle
tess.left(120)
tess.forward(80)
tess.left(120)
tess.forward(80)
tess.left(120) # Complete the triangle

tess.right(180) # Turn tess around
tess.forward(80) # Move her away from the origin

alex.forward(50) # Make alex draw a square
alex.left(90)
alex.forward(50)
alex.left(90)
alex.forward(50)
alex.left(90)
alex.forward(50)
alex.left(90)

wn.exitonclick()
