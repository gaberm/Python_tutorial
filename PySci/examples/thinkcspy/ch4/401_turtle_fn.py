import turtle

def draw_square(tx, sz):
    """Make turtle t draw a square of sz."""
    """ Variatoin to be draw_multicolor_square """
    for i in range(2):
        t.forward(w)
        t.left(90)
        t.forward(h)
        t.left(90)

def draw_rectangle(tx, sz, sz):
    draw_square(tx, sz)

wn = turtle.Screen() # Set up the window and its attributes
wn.bgcolor("lightgreen")
wn.title("Alex meets a function")

tess = turtle.Turtle() # Create tess
tess.pensize(2)
tess.speed(20)
tess.color("red")
tess.hideturtle()
size=10

for i in range(16):
    draw_square(tess, size) # Call the function to draw the square
    #size = size + 2
    tess.forward(10)
    tess.right(30)

wn.exitonclick()
