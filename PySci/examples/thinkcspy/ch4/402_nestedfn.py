import turtle

def draw_rectangle(t, w, h):
    """Get turtle t to draw a rectangle of width w and height h."""
    for i in range(2):
        t.forward(w)
        t.left(90)
        t.forward(h)
        t.left(90)

def draw_square(tx, sz):        # A new version of draw_square
    draw_rectangle(tx, sz, sz)

wn = turtle.Screen() # Set up the window and its attributes
wn.bgcolor("lightgreen")
wn.title("Alex meets a function")

tess = turtle.Turtle() # Create tess
tess.pensize(2)
tess.speed(20)
tess.color("red")
tess.hideturtle()
size=10

for i in range(16):
    draw_square(tess, size) # Call the function to draw the square
    #size = size + 2
    tess.forward(10)
    tess.right(30)

wn.exitonclick()
