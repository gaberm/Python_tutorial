import numpy as np

mylist = [1, 2, 3]
x = np.array(mylist)
x

#just pass in a list directly
y = np.array([4, 5, 6])
y

# Multi-dimensional
m = np.array([[7, 8, 9], [10, 11, 12]])
m.shape
print(m)

#`arange` returns evenly spaced values within a given interval.
n = np.arange(0, 30, 2) # start at 0 count up by 2, stop before 30
n

#`reshape` returns an array with the same data with a new shape.
n = n.reshape(5, 3) # reshape array to be 3x5
n

o = np.linspace(0, 4, 8) # return 9 evenly spaced values from 0 to 4
o.resize(4,2)
np.ones((3, 2))

np.zeros((3,3))

# eye returns a 2-D array with ones on the diagonal and zeros elsewhere
np.eye(3)

np.diag(y)

p = np.ones([2, 3], int)
