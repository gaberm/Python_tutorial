class Person:
    department = 'School of Information' #a class variable

    def set_name(self, new_name): #a method
        self.name = new_name
    def set_location(self, new_location):
        self.location = new_location

person = Person()
person.set_name('Christopher Brooks')
person.set_location('Ann Arbor, MI, USA')
print('{} live in {} and works in the department {}'.format(person.name, person.location, person.department))

# Here's an example of mapping the `min` function between two lists.
store1 = [10.00, 11.00, 12.34, 2.34]
store2 = [9.00, 11.10, 12.34, 2.01]
cheapest = map(min, store1, store2)
list(cheapest)

for item in cheapest:
    print(item)
my_function = lambda a, b, c : a + b * c
my_function(1, 2, 3)

my_list = []
for number in range(0, 1000):
    if number % 2 == 0:
        my_list.append(number)
my_list
my_list = [number for number in range(0,1000) if number % 2 == 0]
