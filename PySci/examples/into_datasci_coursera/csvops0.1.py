import os
import csv

# %precision 2
def print(*args):
    __builtins__.print(*("%.2f" % a if isinstance(a, float) else a
                         for a in args))

os.chdir('C:\\Users\\Mohammed\\gitlab\\Python_tutorial\\examples\\into_datasci_coursera\\')
with open('mpg.csv') as csvfile:
    mpg = list(csv.DictReader(csvfile))

mpg[:2] # The first three dictionaries in our list.

len(mpg)
#`keys` gives us the column names of our csv.
mpg[0].keys()

# Avg consumption in city
sum( float(i['cty'])  for i in mpg) / len(mpg)

# Avg consumption on HWay
sum( float(i['hwy'])  for i in mpg) / len(mpg)

# Check different Cyl numbers
cylinders = set(i['cyl'] for i in mpg)

CityMpgbyCyl=[]
for c in cylinders:
    summmpg= 0
    cyltypecount = 0
    for i in mpg:
        if i['cyl'] == c:
            summmpg += float(i['cty'])
            cyltypecount += 1
    CityMpgbyCyl.append((c, (summmpg/cyltypecount)))

CityMpgbyCyl.sort( key=lambda x: x[0])
CityMpgbyCyl

vechicleClass= set(i['class'] for i in mpg)
HwyMpgClass = []

for c in vechicleClass:
    summmpg= 0
    classCount = 0
    for i in mpg:
        if i['class'] == c:
            summmpg += float(i['hwy'])
            classCount += 1
    HwyMpgClass.append((c, (summmpg/classCount)))

HwyMpgClass.sort(key=lambda x: x[1])                # Ascending
HwyMpgClass.sort(key=lambda x: x[1], reverse=True)  # Descending
HwyMpgClass
