Current Tasks:
- ML Algorithms

Next Tasks:
- ML & Tensor Flow
- PySpark


Future:
- Scala


Reads:
- ML General:
https://pythonprogramminglanguage.com/training-and-test-data/
https://www.udemy.com/data-science-and-machine-learning-with-python-hands-on/?ranMID=39197&ranEAID=oCUR7eOwwME&ranSiteID=oCUR7eOwwME-E1Bt3kv5vbsn72XuPdqoAg&LSNPUBID=oCUR7eOwwME

- Tensor Flow:
https://pythonprogramminglanguage.com/tensorflow/
https://www.udemy.com/complete-guide-to-tensorflow-for-deep-learning-with-python/?ranMID=39197&ranEAID=oCUR7eOwwME&ranSiteID=oCUR7eOwwME-ShCKtrT5ZC9xUgV9TMkEFw&LSNPUBID=oCUR7eOwwME
