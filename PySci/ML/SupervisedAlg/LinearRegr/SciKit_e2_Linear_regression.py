#ex2
# https://www.youtube.com/watch?v=JTj-WgWLKFM



"""#Load Train and Test datasets
#Identify feature and response variable(s) and values must be numeric and numpy arrays


x_train=input_variables_values_training_datasets
y_train=target_variables_values_training_datasets
x_test=input_variables_values_test_datasets

# Create linear regression object
linear = linear_model.LinearRegression()

# Train the model using the training sets and check score
linear.fit(x_train, y_train)
linear.score(x_train, y_train)

#Equation coefficient and Intercept
print('Coefficient: \n', linear.coef_)
print('Intercept: \n', linear.intercept_)

#Predict Output
predicted= linear.predict(x_test)"""

import pandas as pd
import numpy as np
from sklearn import linear_model
from sklearn.model_selection import train_test_split

#Boston house pricing dataset
from sklearn.datasets import load_boston


boston = load_boston()
df_x = pd.DataFrame(boston.data, columns = boston.feature_names)
df_y = pd.DataFrame(boston.target)

df_x.describe()

reg = linear_model.LinearRegression()
x_train, x_test, y_train, y_test = train_test_split(df_x, df_y, test_size=0.2, random_state = 4)
x_train.shape
x_test.shape

reg.fit(x_train, y_train)
reg.coef_

a = reg.predict(x_test)
a[0]
a[1]
a[2]
y_test[0]
y_test

#mean square error
np.mean((a-y_test)**2)
