    # https://www.analyticsvidhya.com/blog/2017/09/common-machine-learning-algorithms/
#Source: https://scikit-learn.org/stable/modules/linear_model.html

#Import Library
#Import other necessary libraries like pandas, numpy...
from sklearn import linear_model

# ex1
# LinearRegression will take in its fit method arrays X, y and will store the coefficients \(w\) of the linear model in its coef_ member:
reg = linear_model.LinearRegression()
reg.fit([[0, 0], [1, 1], [2, 2]], [0, 1, 2])
reg.coef_
reg.intercept_

#ex2
#Iris dataset
from sklearn import linear_model
import numpy as np
from sklearn import datasets
from sklearn.model_selection import train_test_split

iris = datasets.load_iris()
iris.data.shape, iris.target.shape
from sklearn import svm
X_train, X_test, y_train, y_test = train_test_split( iris.data, iris.target, test_size=0.4, random_state=0)
X_train.shape, y_train.shape
X_test.shape, y_test.shape

clf = svm.SVC(kernel='linear', C=1).fit(X_train, y_train)
clf.score(X_test, y_test)

# Computing cross-validated metrics
# The simplest way to use cross-validation is to call the cross_val_score helper function on the estimator and the dataset.
from sklearn.model_selection import cross_val_score
clf = svm.SVC(kernel='linear', C=1)
scores = cross_val_score(clf, iris.data, iris.target, cv=5)
scores

# The mean score and the 95% confidence interval of the score estimate are hence given by:
print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

# By default, the score computed at each CV iteration is the score method of the estimator. It is possible to change this by using the scoring parameter:
from sklearn import metrics
scores = cross_val_score(    clf, iris.data, iris.target, cv=5, scoring='f1_macro')
scores

"""See The scoring parameter: defining model evaluation rules for details. In the case of the Iris dataset, the samples are balanced across target classes hence the accuracy and the F1-score are almost equal.

When the cv argument is an integer, cross_val_score uses the KFold or StratifiedKFold strategies by default, the latter being used if the estimator derives from ClassifierMixin.
"""

# It is also possible to use other cross validation strategies by passing a cross validation iterator instead, for instance:
from sklearn.model_selection import ShuffleSplit
n_samples = iris.data.shape[0]
cv = ShuffleSplit(n_splits=5, test_size=0.3, random_state=0)
cross_val_score(clf, iris.data, iris.target, cv=cv)

# Another option is to use an iterable yielding (train, test) splits as arrays of indices, for example
def custom_cv_2folds(X):
     n = X.shape[0]
     i = 0
     while i <= 2:
         idx = np.arange(n * (i - 1) / 2, n * i / 2, dtype=int)
         yield idx, idx
         i += 1

custom_cv = custom_cv_2folds(iris.data)
cross_val_score(clf, iris.data, iris.target, cv=custom_cv)


"""
def gen():
    i = 0
    while i <=2:
        yield 'hi'
        i+=1

x = gen()
next(x)
"""
