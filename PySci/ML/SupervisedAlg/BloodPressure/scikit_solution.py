#Implementing a solution using SciKit
from sklearn.linear_model import LinearRegression
# LinearRegression uses the gradient descent method
from pandas import read_csv
import matplotlib.pyplot as plt
import os

#Our data
data_path = os.path.join(os.getcwd(), "data/bp_dataset.txt")
dataset = read_csv(data_path, delim_whitespace=True)

X = dataset[['Age']]
y = dataset[['Pressure']]

regr = LinearRegression()
regr.fit(X, y)

# Plot outputs
plt.xlabel('Age')
plt.ylabel('Blood pressure')

plt.scatter(X, y,  color='black')
plt.plot(X, regr.predict(X), color='blue')

plt.show()
plt.gcf().clear()
# plt.plot
# plt.figure()

print( 'Predicted blood pressure at 25 y.o.   = ', regr.predict(25) )
print( 'Predicted blood pressure at 45 y.o.   = ', regr.predict(45) )
print( 'Predicted blood pressure at 27 y.o.   = ', regr.predict(27) )
print( 'Predicted blood pressure at 34.5 y.o. = ', regr.predict(34.5) )
print( 'Predicted blood pressure at 78 y.o.   = ', regr.predict(78) )
