import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn import datasets
import sklearn.model_selection
import numpy as np
import pandas as pd
import random

# import some data to play with
data = datasets.load_iris()
# list(data)
# type(data.data)

trainingSet, testSet =  sklearn.model_selection.train_test_split(data.data, test_size=0.33, random_state=42)
# trainingSet.shape
# testSet.shape

def euclidean_distance(list1, list2):
    from math import sqrt
    from functools import reduce
    # return sqrt(reduce(lambda x,y: x+y, list(map(lambda x, y: ((x-y)**2), remove_flowers(list1), list2))))
    # reduce( lambda x,y : x+y, [1,2,3])

    return sqrt( reduce( lambda x,y: x+y, list(map(lambda x, y: ((x-y)**2), list1, list2 ) ) ) )


def get_neighbours(trainingSet, testSet, k):
    import operator
    # distance = list(map(lambda x, y: euclidean_distance(remove_flowers(x), testSet), trainingSet, testSet))
    distance = list(map(lambda x, y: euclidean_distance(x, testSet), trainingSet, testSet))
    # for every element in the training set, remove the flower from the list
    # and find the distance between the testing set and the item in the training set
    distance = list(zip(trainingSet, distance)) # returns a list of elements and their distances
    distance.sort(key=operator.itemgetter(1))
    neighbors = []
    for x in range(k):
    	neighbors.append(distance[x][0])
    return neighbors


import operator
def getResponse(neighbours):
	classVotes = {}
	for x in neighbours:
		response = x[-1]
		if response in classVotes:
			classVotes[response] += 1
		else:
			classVotes[response] = 1
	sortedVotes = sorted(classVotes.items(), key=operator.itemgetter(1), reverse=True)
	return sortedVotes[0][0]
