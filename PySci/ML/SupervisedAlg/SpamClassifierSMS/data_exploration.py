# Import libraries
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from collections import Counter
import os
from wordcloud import WordCloud

# Load dataset
# input_file = os.path.join(os.pardir, 'datasets', 'spam.csv')
input_file = './datasets/spam.csv'
data = pd.read_csv(input_file, encoding='latin-1', usecols=[0, 1])

# Rename the columns with more explicit names
data.rename(columns={'v1' : 'label', 'v2' : 'message'}, inplace=True)

# Five first rows of the dataset
data.head(5)

# Give a brief description of the dataset
data.groupby('label').describe()

## Bar-plot of the most frequent words in the dataset
# Visualization of the most frequent words of the dataset
count1 = Counter(" ".join(data["message"]).split()).most_common(30)
df1 = pd.DataFrame.from_dict(count1)
df1 = df1.rename(columns={0: "words", 1 : "count"})
fig = plt.figure()
ax = fig.add_subplot(111)
df1.plot.bar(ax=ax, legend = False)
xticks = np.arange(len(df1["words"]))
ax.set_xticks(xticks)
ax.set_xticklabels(df1["words"])
ax.set_ylabel('Number of occurences')
plt.show()

## Wordclouds
#In order to have words occurences in each class, we use wordclouds where most frequent words appear the biggest.
# Wordcloud paramters
wc_height, wc_width = (512, 1024)
wc_bckp_color = 'white'
wc_max_words = 400
wc_max_font_size = 60
random_state = 42
wc_figsize = (12, 10)

# a. Spam class
# Extracting spam messages from DataFrame
spam_df = data.loc[data['label'] == 'spam']

# Creating wordcloud for spam
spam_wc = WordCloud(
    height=wc_height, width=wc_width, background_color=wc_bckp_color,
    max_words=wc_max_words, max_font_size=wc_max_font_size,
    random_state=random_state
).generate(str(spam_df['message']))

# Display the wordcloud
fig = plt.figure(figsize=wc_figsize)
plt.imshow(spam_wc)
plt.axis('off')
plt.show()


# b. Ham class
# Extracting ham messages from DataFrame
ham_df = data.loc[data['label'] == 'ham']

# Creating wordcloud for ham
ham_wc = WordCloud(
    height=wc_height, width=wc_width, background_color=wc_bckp_color,
    max_words=wc_max_words, max_font_size=wc_max_font_size,
    random_state=random_state
).generate(str(ham_df['message']))

# Display the wordcloud
fig = plt.figure(figsize=wc_figsize)
plt.imshow(ham_wc)
plt.axis('off')
plt.show()
