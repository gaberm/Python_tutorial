#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Libraries import!!
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
# # Trying Fuzzy Wuzzy
from fuzzywuzzy import fuzz # fuzz is used to compare TWO strings
from fuzzywuzzy import process # process is used to compare a string to MULTIPLE other strings
from tabulate import tabulate # Used for Charts graphical representation
import re   # RegEX
import timeit  # Timing module
import gc
#Environment defintion!!
pd.options.mode.chained_assignment = None  # default='warn'

#Loading Vars
chunksize = 10
fname = 'C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\input\\input_small.csv'

def LoadTable(fname):
    datareader = pd.read_table(fname, sep=',', usecols=["Message"],
                        iterator=True, engine= 'c', names =["Time", "Source", "Message"], parse_dates = True)#, chunksize=1000,)
    dataset = pd.read_csv(fname, usecols=['Message'], index_col=None,  dtype={"Message": str}, engine='c' )
    return dataset
    # print("\n The Load Table..\n", table.head(20))
pwd


dataset=LoadTable(fname)
