import numpy as np
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from fuzzywuzzy import process

correct_roadnames = ["Aljunied Avenue 1", "Aljunied Avenue 2", "Longdrive Ave","Oxford Circus", "Charlie Temple"  ]

"""
def correct_road(roadname):
    new_name, score = process.extractOne(roadname, correct_roadnames)
    if score < 90:
        print(roadname,score)
        return roadname, score
    else:
        print(new_name,score)
        return new_name, score
"""



#df['corrected'], df['score'] = zip(*df['name'].apply(correct_road))

def correct_road(roadname):
    if roadname in correct_roadnames:  # might want to make this a dict for O(1) lookups
        return roadname, 100

    new_name, score = process.extractOne(roadname, correct_roadnames)
    if score < 90:
        return roadname, score
    else:
        return new_name, score

df['corrected'], df['score'] = zip(*df['name'].apply(correct_road))
correct_road("Longdrive")
